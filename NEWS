JOH -- history of user-visible changes. 2011-03-31
Copyright (C) 2011 Sergey Poznyakoff
See the end of file for copying conditions.

Please report bugs and send suggestions to <gray+joh@gnu.org.ua>.

Version 1.3, 2011-03-31

* Use IPv6 URLs as proposed by RFC 2732

The address part of an IPv6 URL must be either an IPv6 address in
numeric notation *enclosed in square brackets* or a host name, e.g.:

  inet6://[::1]:1100

  

Version 1.2, 2011-02-17

* Implement special handling for GET requests.

An incoming GET request usually means that someone has pointed his web
browser to the URL served by johd or joh.cgi.  Both programs can be
configured to either return a 404 response with a configurable HTML
content, or to redirect the request to another web server.


Version 1.1, 2011-02-03

* Support for "HTTP CONNECT" proxying.

* ACLs for requested Jabber servers.

Addresses of the requested Jabber servers are verified via TCP
wrappers.  The daemon name is built using the following schema:

   <srvname>/jabber@<ipaddr>

where <srvname> is the current daemon name (as set up using the
-S option, or, if it was not used, the default "johd"), and
<ipaddr> is the IP address of the Jabber server.  For example,
to allow access to server  213.130.31.41 for all incoming
connections, use the following line in your /etc/hosts.allow
(assuming default daemon name):

  johd/jabber@213.130.31.41: ALL
 

Version 1.0, 2011-01-29

First stable release.


=========================================================================
Copyright information:

Copyright (C) 2011 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.

Local variables:
mode: outline
paragraph-separate: "[	]*$"
eval: (add-hook 'write-file-hooks 'time-stamp)
time-stamp-start: "changes. "
time-stamp-format: "%:y-%02m-%02d"
time-stamp-end: "\n"
end:
