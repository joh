/* SID support for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

#define IDSEQLEN      60
#define IDTIMLEN      62

/* Create new session ID. */
void
new_session_id(joh_sessid_t idbuf)
{
	time_t t;
	struct tm *tm;
	static const char xchr[] =
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static unsigned seq = 0;

	time(&t);
	tm = gmtime(&t);
	idbuf[0] = xchr[tm->tm_year % IDSEQLEN];
	idbuf[1] = xchr[tm->tm_mon];
	idbuf[2] = xchr[tm->tm_mday];
	idbuf[3] = xchr[tm->tm_hour];
	idbuf[4] = xchr[tm->tm_min % IDTIMLEN];
	idbuf[5] = xchr[tm->tm_sec % IDTIMLEN];
	idbuf[6] = xchr[seq / IDSEQLEN];
	idbuf[7] = xchr[seq % IDSEQLEN];
	idbuf[8] = 0;
	seq++;
	if (seq >= IDSEQLEN * IDSEQLEN)
		seq = 0;
}
