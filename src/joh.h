/* JOH -- Jabber Over HTTP proxy.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <limits.h>
#include <signal.h>
#include <time.h>

#include "grecs.h"

#ifndef JOH_ARG_UNUSED
# define JOH_ARG_UNUSED __attribute__ ((__unused__))
#endif

#ifndef JOH_PRINTFLIKE
# define JOH_PRINTFLIKE(fmt,narg) __attribute__ ((__format__ (__printf__, fmt, narg)))
#endif

#ifndef JOH_DEPRECATED
# define JOH_DEPRECATED __attribute__ ((deprecated))
#endif

#define DEFAULT_PORT_STRING "1100"
#define JABBER_SERVER_NAME "gnu.org.ua"
#define JABBER_PORT_STRING "5222"
#define JABBER_SERVER "inet://" JABBER_SERVER_NAME

#define FDSET_RD 0
#define FDSET_WR 1

#define _FDSET_MAX 2

#define FDMASK(n) (1<<(n))

#define FDMASK_RD FDMASK(FDSET_RD)
#define FDMASK_WR FDMASK(FDSET_WR)

#define IOBUFSIZE 4096
#define STR(s) ((s) ? (s) : "(null)")

/* error.c: Diagnostics */
extern const char *program_name;
extern const char *log_tag_option;

extern const char *syslog_tag;
extern int syslog_include_prio;
extern int syslog_facility;
extern int syslog_source_info;

extern char *pidfile;
extern int daemon_option;
extern char *user_name;

#define JOH_DEBCAT_MAIN   0
#define JOH_DEBCAT_HTTP   1
#define JOH_DEBCAT_PROTO  2
#define JOH_DEBCAT_SESS   3
#define JOH_DEBCAT_CFGRAM 4
#define JOH_DEBCAT_CFLEX  5
#define _JOH_DEBCAT_MAX   6

void set_program_name(const char *arg);
void joh_initlog(void);
int string_to_syslog_facility(const char *str, int *pfac);
int parse_debug_level(const char *arg);
void logmsg(int severity, const char *file, unsigned line,
	    const char *fmt, ...) JOH_PRINTFLIKE(4,5);

void verror(const char *fmt, va_list ap);
void error(const char *fmt, ...) JOH_PRINTFLIKE(1,2);
void die(int code, const char *fmt, ...) JOH_PRINTFLIKE(2,3);
void vwarn(const char *fmt, va_list ap); 
void warn(const char *fmt, ...) JOH_PRINTFLIKE(1,2);
void debugf(const char *fmt, ...) JOH_PRINTFLIKE(1,2);
void debug_begin_out(char *file, int line);

extern int debug_level[];
extern int source_info_option;
extern int log_to_stderr;

#define debug(cat, lev, s)						\
	do {								\
		if (debug_level[cat] >= (lev)) {			\
			debug_begin_out(__FILE__,__LINE__);		\
			debugf s;					\
		}							\
	} while (0)

#define JOH_VBUFSIZE 69

size_t joh_format_vbuf(char vbuf[JOH_VBUFSIZE], const char *buf, size_t size);
void joh_logdump(char **pfx, const char *buf, size_t size);


/* mem.c: Memory management */
void xalloc_die();
void *xmalloc(size_t size);
void *xrealloc(void *ptr, size_t size);
void *xcalloc(size_t nmemb, size_t size);
void *xzalloc(size_t size);
char *xstrdup(char *str);


/* netaddr.c: Network address functions */
enum joh_client_class {
	client_unused,
	client_cgi,
	client_http,
	client_transparent
};

struct joh_http_get_reply {
	int redirect; /* Redirect or error */
	char *data;   /* Redirection target or contents of the 404 page. */
	size_t length;/* Length of data */
};

struct joh_sockaddr {
	struct joh_sockaddr *prev, *next;
	struct sockaddr *addr;            /* Address */
	socklen_t        addrlen;         /* Size of addr */
	char *str;                        /* string representation of addr,
					     filled up by joh_sockaddr_str */
	struct joh_session *sess;         /* Associated session, if any */
	enum joh_client_class class;      /* Client class */
	const char *srvname;              /* Sevice name for TCP wrappers */
	struct joh_http_get_reply *reply; /* Reply for GET requests */ 
};

#define JOH_HINT_BIND 0x01
#define JOH_HINT_SERVICE 0x02
#define JOH_HINT_CLASS 0x04
#define JOH_HINT_SRVNAME 0x08
#define JOH_HINT_REPLY 0x10

struct joh_sockaddr_hints {
	char *name;
	int flags;
	char *service;
	enum joh_client_class class;
	char *srvname;
	struct joh_http_get_reply *reply;
};

struct joh_sockaddr *joh_sockaddr_new(socklen_t len);
struct joh_sockaddr *joh_sockaddr_copy(struct joh_sockaddr *src);
struct joh_sockaddr *joh_sockaddr_append(struct joh_sockaddr *a,
					 struct joh_sockaddr *b);
void joh_sockaddr_free(struct joh_sockaddr *sp);
struct joh_sockaddr *parse_sockaddr(const char *arg,
				    struct joh_sockaddr_hints *jh);
const char *joh_sockaddr_str(struct joh_sockaddr *sa);
char *sockaddr_str(struct sockaddr *sa, socklen_t salen);


/* cbuf.c: Character buffers. */
typedef struct joh_cbuf *joh_cbuf_t;
#define JOH_CBUF_BUCKET_SIZE 1024

joh_cbuf_t joh_cbuf_create();
void joh_cbuf_clear(joh_cbuf_t buf);
void joh_cbuf_free(joh_cbuf_t buf);
void joh_cbuf_append(joh_cbuf_t buf, const void *str, size_t n);
void joh_cbuf_append_char(joh_cbuf_t buf, char c);
size_t joh_cbuf_size(joh_cbuf_t buf);
size_t joh_cbuf_coalesce(joh_cbuf_t buf);
void *joh_cbuf_head(joh_cbuf_t buf, size_t *psize);
void *joh_cbuf_finish(joh_cbuf_t buf);

/* base64.c */
int base64_encode(const unsigned char *input, size_t input_len,
		  unsigned char **output, size_t *output_len);
int base64_decode(const unsigned char *input, size_t input_len,
		  unsigned char **output, size_t *output_len);


/* sessid.c: session ids */
typedef char joh_sessid_t[9];
void new_session_id(joh_sessid_t);

/* conntab.c: Connection types: */
enum joh_conntype {
	conn_unused,   /* unused slot */
	conn_master,   /* master connection */
	conn_jabber,   /* connection to jabber server */
	conn_client,   /* connection from client */
};
#define JOH_CONNTYPE_MAX 4

extern char *conn_type_str[];
extern char *client_class_str[];

int getmaxfd(void);
void joh_conntab_init(void);
enum joh_conntype joh_connection_type(int fd);
enum joh_client_class joh_connection_client_class(int fd);
struct joh_sockaddr *joh_connection_address(int fd);
void joh_connection_register(int fd, enum joh_conntype type,
			     struct joh_sockaddr *sa);
void joh_connection_iterate(int (*itr)(int fd, enum joh_conntype type,
				       struct joh_sockaddr *sa, void *),
			    void *data,
			    int all);
void joh_connection_inset(int fd, int inset);


/* sess.c: session support */

enum joh_http_type {
	joh_http_unspecified,
	joh_http_poll,
	joh_http_connect,
	joh_http_transparent,
	joh_http_get
};

enum joh_param_key {
	joh_param_id,       /* session identifier */  
	joh_param_key,      /* key */
	joh_param_newkey,   /* new key, if any */
	joh_param_payload,  /* payload */
	joh_param_server,
	joh_param_port
};
#define joh_param_max 6

struct joh_param {
	char *ptr;
	size_t size;
};

struct joh_obuf {
	char *base;  /* base pointer */
	int base_alloc; /* base was allocated */
	size_t size; /* number of octets in base */
	size_t off;  /* offset of the current position in base. */
};

enum joh_client_dir {
	joh_client_none,
	joh_client_in,
	joh_client_out
};

struct joh_session {
	enum joh_conntype type; /* Either conn_jabber or conn_client. */
	
	joh_cbuf_t ibuf;        /* Input buffer */
	struct joh_obuf obuf;   /* Output buffer */

	int fd;                 /* transport fd */
	struct joh_sockaddr *sa;/* corresponding sockaddr */
	
	/* TTL info */
	time_t last_used;   /* time last used */
	/* Sessions form a doubly-linked list in LIFO fashion, with
	   the last recently used session in its head. */
	struct joh_session *prev, *next;

	/* A peer session (conn_jabber for client, and conn_client for
	   jabber) */
	struct joh_session *peer;
	
	struct joh_param param[joh_param_max];
	void *decstate; /* Temporary storage for decoder */
	enum joh_http_type http_type;
};

void joh_session_unlink(struct joh_session *sess);
void joh_session_promote(struct joh_session *sess);
struct joh_session *joh_session_lookup(struct joh_session *);
struct joh_session *joh_session_create(int fd, enum joh_conntype type,
				       struct joh_sockaddr *sa);
void joh_session_destroy(struct joh_session *sess);
struct timeval *joh_session_timeout(struct timeval *tv);
void joh_session_set_timeout(enum joh_conntype type, time_t timeout);

int joh_client_session_setup(enum joh_client_dir dir, struct joh_session *sess);
int joh_client_session_flush(enum joh_client_dir dir, struct joh_session *sess);



/* socket.c: socket operations */
extern fd_set joh_fd_set[_FDSET_MAX];
extern int joh_max_fd;

void joh_add_fd(int fd, int mask);
void joh_rem_fd(int fd);
void joh_chg_fd(int fd, int mask);

void joh_listen_socket_setup(struct joh_sockaddr *sa);
void joh_listen_socket_cleanup();
int new_jabber_connection(struct joh_session *sess, struct joh_sockaddr **pjsa);


/* acl.c */
#ifdef USE_TCPWRAP
int connection_allowed(int fd, const char *srvname);
int destination_allowed(const char *srvname,
			struct sockaddr *sa_clt, struct sockaddr *sa_srv);
#else
# define connection_allowed(fd,srv) 1
# define destination_allowed(srvname,clt,srv) 1
#endif


/* johd.c */
extern struct joh_sockaddr_hints listen_hints;
extern struct joh_sockaddr_hints default_listen_hints;
extern struct joh_sockaddr_hints jabber_hints;
extern struct joh_sockaddr *jabber_addr;
extern struct joh_sockaddr *listen_addrs;

/* pdu.c */
extern char *param_str[];

int joh_pdu_decode_buf(struct joh_param param[joh_param_max],
		       joh_cbuf_t ibuf, const char *id);
int joh_pdu_encode_buf(struct joh_obuf *buf, struct joh_param *param);
int joh_pdu_decode(struct joh_session *sess);
int joh_pdu_encode(struct joh_session *sess, struct joh_obuf *buf,
		   struct joh_param *param);

/* selloop.c */
int joh_loop(void);

/* key,c */
int joh_validate_key(struct joh_session *hs, struct joh_session *js);

/* interp.c */
int joh_data_decode(struct joh_session *sess);
int joh_data_encode(struct joh_sockaddr *sa,
		    struct joh_obuf *buf, struct joh_param *param);


/* http.c */
int joh_http_decode(struct joh_session *sess);
int joh_http_encode(struct joh_session *sess, struct joh_obuf *buf,
		    struct joh_param *param);

/* util.c */
void store_param(struct joh_param *param, char *value, size_t len);
int unescape_query(char *q);
int parse_post_query(struct joh_param *param, char *query);
int parse_connect_query(struct joh_param *param, char *query);
int parse_content(struct joh_param *param, char *content_ptr,
		  size_t content_length);
int joh_param_check(struct joh_session *sess);

void joh_obuf_free(struct joh_obuf *obuf);
int strprefix(const char *str, const char *pfx);


/* poll.c */
int joh_http_poll_setup(enum joh_client_dir dir, struct joh_session *sess);
int joh_http_poll_flush(enum joh_client_dir dir, struct joh_session *sess);


/* transparent.c */
int joh_transparent_decode(struct joh_session *sess);
int joh_transparent_encode(struct joh_session *sess,
			   struct joh_obuf *obuf, struct joh_param *param);
int joh_http_transparent_flush(enum joh_client_dir dir,
			       struct joh_session *sess);
int joh_http_connect_flush(enum joh_client_dir dir, struct joh_session *sess);

int joh_http_get_flush(enum joh_client_dir dir, struct joh_session *sess);

/* hintab.c */
struct joh_sockaddr_hints *joh_get_hint_by_name(const char *name, int install);
void new_get_reply(struct joh_sockaddr_hints *hints, int redirect,
		   const char *text);

/* config.c */
void config_help(void);
void config_init(void);
void config_finish(struct grecs_node *tree);













