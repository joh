/* Transparent proxy data interpreter.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

int
joh_transparent_decode(struct joh_session *sess)
{
	size_t size;
	
	if (sess->param[joh_param_payload].ptr)
		free(sess->param[joh_param_payload].ptr);

	size = joh_cbuf_size(sess->ibuf);
	sess->param[joh_param_payload].ptr = xmalloc(size);
	memcpy(sess->param[joh_param_payload].ptr,
	       joh_cbuf_finish(sess->ibuf), size);	
	sess->param[joh_param_payload].size = size;
	return 0;
}

int
joh_transparent_encode(struct joh_session *sess,
		       struct joh_obuf *obuf, struct joh_param *param)
{
	obuf->base = param[joh_param_payload].ptr;
	obuf->base_alloc = 0;
	obuf->size = param[joh_param_payload].size;
	obuf->off = 0;
	return 0;
}

int
joh_http_connect_flush(enum joh_client_dir dir, struct joh_session *sess)
{
	struct joh_session *js;
	struct joh_sockaddr *jsa;
	int fd;

	switch (dir) {
	case joh_client_none:
		break;
		
	case joh_client_in:
		joh_chg_fd(sess->fd, FDMASK_RD|FDMASK_WR);
		
		fd = new_jabber_connection(sess, &jsa);
		if (fd == -1)
			return -1;
	
		js = joh_session_create(fd, conn_jabber, jsa);
		debug(JOH_DEBCAT_SESS, 1, ("created jabber session @ %s",
					   joh_sockaddr_str(jsa)));
		       
		jsa->sess = js;
		sess->peer = js;
		js->peer = sess;
		joh_connection_register(fd, conn_jabber, jsa);
		joh_chg_fd(fd, FDMASK_RD);
		break;
		
	case joh_client_out:
		debug(JOH_DEBCAT_SESS, 1,
		      ("%s: switching to transparent mode",
		       joh_sockaddr_str(sess->sa)));
		sess->sa->class = client_transparent;
		sess->http_type = joh_http_transparent;
		joh_session_unlink(sess);
		joh_session_unlink(sess->peer);
		joh_chg_fd(sess->fd, FDMASK_RD);
		joh_obuf_free(&sess->obuf);
	}
	return 0;
}

int
joh_http_transparent_flush(enum joh_client_dir dir, struct joh_session *sess)
{
	struct joh_session *js = sess->peer;
	int i;
	
	switch (dir) {
	case joh_client_none:
		break;
		
	case joh_client_in:
		if (sess->param[joh_param_payload].size) {
			joh_chg_fd(js->fd, FDMASK_RD|FDMASK_WR);
			js->obuf.base = sess->param[joh_param_payload].ptr;
			js->obuf.base_alloc = 0;
			js->obuf.size = sess->param[joh_param_payload].size;
			js->obuf.off = 0;
		} else
			joh_chg_fd(js->fd, FDMASK_RD);
		break;
		
	case joh_client_out:
		joh_chg_fd(sess->fd, FDMASK_RD);
		joh_obuf_free(&sess->obuf);
		for (i = 0; i < joh_param_max; i++) {
			free(sess->param[i].ptr);
			sess->param[i].ptr = NULL;
			sess->param[i].size = 0;
		}
		
		break;
	}
	return 0;
}


