/* XEP-0025 keys for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"
#include "sha1.h"

int
joh_validate_key(struct joh_session *hs, struct joh_session *js)
{
	int res;
	
	if (!js->param[joh_param_key].ptr) {
		js->param[joh_param_key].ptr =
			xzalloc(hs->param[joh_param_key].size + 1);
		js->param[joh_param_key].size =
			hs->param[joh_param_key].size;
		res = 1;
	} else {
		unsigned char sha1digest[20];
		struct sha1_ctx sha1context;
		char *newkey;
		size_t newkeylen;
		
		sha1_init_ctx(&sha1context);
		sha1_process_bytes(hs->param[joh_param_key].ptr,
				   hs->param[joh_param_key].size,
				   &sha1context);
		sha1_finish_ctx(&sha1context, sha1digest);
		
		if (base64_encode(sha1digest, sizeof(sha1digest),
				  (unsigned char **)&newkey,
				  &newkeylen))
			xalloc_die();
		
		debug(JOH_DEBCAT_PROTO, 3,
		      ("prev key=%s, new key=%s, exp key=%s",
			  js->param[joh_param_key].ptr, newkey,
			  hs->param[joh_param_key].ptr));
		
		res = newkeylen == js->param[joh_param_key].size &&
			memcmp(newkey, js->param[joh_param_key].ptr,
			       newkeylen) == 0;
		free(newkey);
	}
	if (res == 1)
		memcpy(js->param[joh_param_key].ptr,
		       hs->param[joh_param_key].ptr,
		       js->param[joh_param_key].size + 1);
	return res;
}
		
