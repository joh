/* Diagnostic functions for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"
#include <ctype.h>
#include <string.h>
#include <syslog.h>

struct grecs_list *cfg_debug_spec;

static int
cb_syslog_facility(enum grecs_callback_command cmd,
		   grecs_locus_t *locus,
		   void *varptr,
		   grecs_value_t *value,
		   void *cb_data)
{
	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, _("Unexpected block statement"));
		return 1;
	}
	if (!value || value->type != GRECS_TYPE_STRING) {
		grecs_error(locus, 0, _("expected string argument"));
		return 1;
	}

	if (string_to_syslog_facility(value->v.string, varptr))
		grecs_error(locus, 0, _("Unknown syslog facility `%s'"),
			    value->v.string);
	return 0;
}

static struct grecs_keyword syslog_kw[] = {
	{ "facility", N_("name"),
	  N_("Set syslog facility. Arg is one of the following: user, daemon, "
	     "auth, authpriv, mail, cron, local0 through local7 (case-insensitive), "
	     "or a facility number."),
	  grecs_type_string, GRECS_AGGR, &syslog_facility, 0,
	  cb_syslog_facility },
	{ "tag", N_("string"), N_("Tag syslog messages with this string"),
	  grecs_type_string, GRECS_DFLT, &syslog_tag },
	{ "print-priority", N_("arg"),
	  N_("Prefix each message with its priority"),
	  grecs_type_bool, GRECS_DFLT, &syslog_include_prio },
	{ NULL },
};

static int
cb_class_type(enum grecs_callback_command cmd,
	      grecs_locus_t *locus,
	      void *varptr,
	      grecs_value_t *value,
	      void *cb_data)
{
	struct joh_sockaddr_hints *hints = varptr;
	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, _("Unexpected block statement"));
		return 1;
	}
	if (!value || value->type != GRECS_TYPE_STRING) {
		grecs_error(locus, 0, _("expected string argument"));
		return 1;
	}

	if (strcasecmp(value->v.string, "HTTP") == 0) {
		hints->class = client_http;
		hints->service = "80";
	} else if (strcasecmp(value->v.string, "CGI") == 0) {
		hints->class = client_cgi;
		hints->service = DEFAULT_PORT_STRING;
	} else {
		grecs_error(locus, 0,
			    "unrecognized client class: %s",
			    value->v.string);
	}
	hints->flags |= JOH_HINT_SERVICE;
	return 0;
}

static int
cb_http_common(int redirect,
	       enum grecs_callback_command cmd,
	       grecs_locus_t *locus,
	       void *varptr,
	       grecs_value_t *value,
	       void *cb_data)
{
	struct joh_sockaddr_hints *hints = varptr;

	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, _("Unexpected block statement"));
		return 1;
	}
	if (!value || value->type != GRECS_TYPE_STRING) {
		grecs_error(locus, 0, _("expected string argument"));
		return 1;
	}
	new_get_reply(hints, redirect, value->v.string);
	return 0;
}

static int
cb_class_http_error_page(enum grecs_callback_command cmd,
			 grecs_locus_t *locus,
			 void *varptr,
			 grecs_value_t *value,
			 void *cb_data)
{
	return cb_http_common(0, cmd, locus, varptr, value, cb_data);
}

static int
cb_class_http_redirect(enum grecs_callback_command cmd,
			 grecs_locus_t *locus,
			 void *varptr,
			 grecs_value_t *value,
			 void *cb_data)
{
	return cb_http_common(1, cmd, locus, varptr, value, cb_data);
}

/*
  class NAME {
    type CGI|HTTP;
    service NAME;
    http-error-page FILE;
    http-redirect URL;
  }
*/
static struct grecs_keyword class_kw[] = {
	{ "type", "<arg: CGI | HTTP>", "Set class type (default: HTTP)",
	  grecs_type_string, GRECS_AGGR, NULL, 0, cb_class_type },
	{ "service", NULL, "Service name for TCP wrappers",
	  grecs_type_string, GRECS_AGGR, NULL,
	  offsetof(struct joh_sockaddr_hints, srvname) },
	{ "http-error-page", "file", "Read HTTP error page from <file>",
	  grecs_type_string, GRECS_AGGR, NULL, 0,
	  cb_class_http_error_page },
	{ "http-redirect", "URL", "Redirect HTTP GET requests to URL",
	  grecs_type_string, GRECS_AGGR, NULL, 0,
	  cb_class_http_redirect },
	{ NULL }
};

static int
cb_class(enum grecs_callback_command cmd,
	 grecs_locus_t *locus,
	 void *varptr,
	 grecs_value_t *value,
	 void *cb_data)
{
	struct joh_sockaddr_hints *hints;
	void **pdata = cb_data;

	switch (cmd) {
	case grecs_callback_section_begin:
		if (!value || value->type != GRECS_TYPE_STRING) {
			grecs_error(locus, 0, _("tag must be a string"));
			return 0;
		}
		hints = joh_get_hint_by_name(value->v.string, 1);
		*pdata = hints;
		break;
	case grecs_callback_section_end:
		hints = *pdata;
		if (hints->class == client_unused) 
			hints->class = client_http;
		hints->flags |= JOH_HINT_CLASS;
		if (hints->srvname)
			hints->flags |= JOH_HINT_SRVNAME;

		break;
	case grecs_callback_set_value:
		grecs_error(locus, 0, _("invalid use of block statement"));
	}
	return 0;
}


static int
cb_listen(enum grecs_callback_command cmd,
	  grecs_locus_t *locus,
	  void *varptr,
	  grecs_value_t *value,
	  void *cb_data)
{
	char *url;
	struct joh_sockaddr_hints *hints = &listen_hints;
	
	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, _("Unexpected block statement"));
		return 1;
	}
	if (!value || value->type == GRECS_TYPE_LIST) {
		grecs_error(locus, 0, _("expected string argument"));
		return 1;
	}
	if (value->type == GRECS_TYPE_STRING)
		url = value->v.string;
	else if (value->v.arg.c > 2) {
		grecs_error(locus, 0, _("too many argument"));
		return 1;
	} else {
		if (value->v.arg.v[0]->type != GRECS_TYPE_STRING) {
			grecs_error(locus, 0, _("URL must be a string"));
			return 1;
		}
		url = value->v.arg.v[0]->v.string;

		if (value->v.arg.c == 2) {
			if (value->v.arg.v[1]->type != GRECS_TYPE_STRING) {
				grecs_error(locus, 0,
					    _("class name must be a string"));
				return 1;
			}
			hints =
			  joh_get_hint_by_name(value->v.arg.v[1]->v.string, 0);
			if (!hints) {
				grecs_error(locus, 0,
					    _("class %s not defined"),
					    value->v.arg.v[1]->v.string);
				return 1;
			}
		}
	}

        listen_addrs = joh_sockaddr_append(listen_addrs,
					   parse_sockaddr(url, hints));
	return 0;
}
		
static int
cb_server(enum grecs_callback_command cmd,
	  grecs_locus_t *locus,
	  void *varptr,
	  grecs_value_t *value,
	  void *cb_data)
{
	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, _("Unexpected block statement"));
		return 1;
	}
	if (!value || value->type != GRECS_TYPE_STRING) {
		grecs_error(locus, 0, _("expected string argument"));
		return 1;
	}
        jabber_addr = parse_sockaddr(value->v.string, &jabber_hints);
        if (jabber_addr->next) {
		warn("%s resolves to more than one address, using %s",
		     optarg, joh_sockaddr_str(jabber_addr));
		joh_sockaddr_free(jabber_addr->next);
		jabber_addr->next = NULL;
	}
	return 0;
}

static void
set_timeout(enum joh_conntype sesstype, char *arg, grecs_locus_t *loc)
{
	time_t timeout;
	int ind = -1;
	static struct { char s[3]; unsigned f; } tab[] = {
		{ "hH", 3600 },
		{ "mM", 60 },
		{ "sS", 1 }
	};

	if (sesstype == conn_unused) {
		switch (*arg) {
		case 'j':
		case 'J':
			sesstype = conn_jabber;
			break;
		case 'c':
		case 'C':
			sesstype = conn_client;
			break;
		default:
			grecs_error(loc, 0,
				    "bad type in timeout specification: %s",
				    arg);
			return;
		}
		if (*++arg != ':') {
			grecs_error(loc, 0,
				    "timeout specification missing colon: %s",
				    arg);
			return;
		}
	}
	
	timeout = 0;
	for (;;) {
		unsigned long t = strtoul(arg + 1, &arg, 10);
		if (*arg) {
			unsigned x = 0;
			
			for (ind++; ind<sizeof(tab)/sizeof(tab[0]);
			     ind++) {
				if (strchr(tab[ind].s, *arg)) {
					x = tab[ind].f;
					break;
				}
			}
			if (x == 0) {
				grecs_error(loc, 0, "unknown time suffix %c",
					    *arg);
				return;
			}
			timeout += t * x;
			if (x > 1)
				continue;
		}
		break;
	}
	if (*arg) {
		grecs_error(loc, 0, "invalid timeout specification: %s", arg);
		return;
	}
	joh_session_set_timeout(sesstype, timeout);
}

static int
cb_timeout(enum grecs_callback_command cmd,
	   grecs_locus_t *locus,
	   void *varptr,
	   grecs_value_t *value,
	   void *cb_data)
{
	enum joh_conntype session = conn_unused;
	char *timestr;
	
	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, _("Unexpected block statement"));
		return 1;
	}
	if (!value) {
		grecs_error(locus, 0, _("required arguments missing"));
		return 1;
	}
	if (value->type == GRECS_TYPE_STRING)
		timestr = value->v.string;
	else if (value->type == GRECS_TYPE_ARRAY && value->v.arg.c == 2) {
		char *s;

		if (value->v.arg.v[0]->type != GRECS_TYPE_STRING) {
			grecs_error(locus, 0,
				    _("session type not a string"));
			return 1;
		}
		if (value->v.arg.v[1]->type != GRECS_TYPE_STRING) {
			grecs_error(locus, 0,
				    _("timeout not a string"));
			return 1;
		}
		s = value->v.arg.v[0]->v.string;
		if (strcmp(s, "j") || strcmp(s, "jabber")) {
			session = conn_jabber;
		} else if (strcmp(s, "c") || strcmp(s, "client")) {
			session = conn_client;
		} else {
			grecs_error(locus, 0, _("unrecognized session type"));
			return 1;
		}
		timestr = value->v.arg.v[1]->v.string;
	} else {
		grecs_error(locus, 0, _("invalid arguments"));
		return 1;
	}
	
	set_timeout(session, timestr, locus);
	return 0;
}
	
static struct grecs_keyword joh_kw[] = {
	{ "debug", NULL, "Set debug level",
	  grecs_type_string, GRECS_LIST, &cfg_debug_spec },
	{ "syslog", NULL, "Configure syslog logging",
	  grecs_type_section, GRECS_DFLT, NULL, 0, NULL, NULL, syslog_kw },
	{ "line-info", NULL,
	  "Include source line information in debugging messages",
	  grecs_type_bool, GRECS_DFLT, &syslog_source_info },
	{ "pidfile", "file", "Write PID to <file>",
	  grecs_type_string, GRECS_DFLT, &pidfile },
	{ "standalone", NULL, "Standalone mode",
	  grecs_type_bool, GRECS_DFLT, &daemon_option },
	{ "user", NULL, "Run as this user",
	  grecs_type_string, GRECS_DFLT, &user_name },
	{ "class", "name: string", "Define socket class",
	  grecs_type_section, GRECS_DFLT, NULL, 0, cb_class, NULL, class_kw },
	{ "timeout",
	  "session: c(lient) or j(abber)> <arg: timespec",
	  "Set session timeout",
	  grecs_type_string, GRECS_AGGR, NULL, 0, cb_timeout },
	{ "listen", "url: string> <class: name", "Listen on a socket",
	  grecs_type_string, GRECS_MULT, NULL, 0, cb_listen },
	{ "server", "url", "Set Jabber server URL",
	  grecs_type_string, GRECS_DFLT, NULL, 0, cb_server },
	{ NULL }
};

void
config_help()
{
	static char docstring[] =
		N_("Configuration file structure for johd.\n"
		   "For more information, see johd(8).");
	grecs_print_docstring(docstring, 0, stdout);
	grecs_print_statement_array(joh_kw, 1, 0, stdout);
}

void
config_init()
{
	grecs_include_path_setup(DEFAULT_VERSION_INCLUDE_DIR,
				 DEFAULT_INCLUDE_DIR, NULL);
	grecs_preprocessor = DEFAULT_PREPROCESSOR;
	grecs_log_to_stderr = 1;
}

void
flush_debug_levels(struct grecs_list *lp)
{
	struct grecs_list_entry *ep;

	if (!lp)
		return;
	for (ep = lp->head; ep; ep = ep->next) {
		if (parse_debug_level(ep->data))
			die(EX_CONFIG,
			    "invalid debugging category or level: %s",
			    (char*)ep->data);
	}
	grecs_list_free(lp);
}

void
config_finish(struct grecs_node *tree)
{
	grecs_tree_reduce(tree, joh_kw, GRECS_AGGR);
	if (grecs_tree_process(tree, joh_kw))
		exit(EX_CONFIG);
	flush_debug_levels(cfg_debug_spec);
	grecs_tree_free(tree);
}
	
			
	
