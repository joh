/* HTTP data interpreter.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

enum sep_state {
	sep_init,
	sep_cr1,
	sep_lf1,
	sep_cr2,
	sep_lf2
};

struct http_decode_state {
	enum sep_state state;
	size_t off;
	size_t content_length;
};

static char *http_type_str[] = {
	"unknown",
	"POST",
	"CONNECT",
	"[tunnel]",
	"GET"
};

static struct http_decode_state *
get_state(void **pstate)
{
	struct http_decode_state *p;
	if (!pstate)
		return NULL;
	if (!*pstate) {
		p = xzalloc(sizeof(*p));
		*pstate = p;
	} else
		p = *pstate;
	return p;
}

static int transtab[][255] = {
	{  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, },
	{  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, },
	{  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  3,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, },
	{  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  4,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 
	   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, }
};

enum sep_state 
find_separator(enum sep_state state,
	       const unsigned char *iptr, size_t isize,
	       size_t off, size_t *poff)
{
	iptr += off;
	isize -= off;

	for (; isize && state != sep_lf2; iptr++, isize--, off++)
		state = transtab[state][*iptr];
	
	*poff = off;
	return state;
}

static int
find_eol(const char *text, char **end)
{
	enum sep_state state = sep_init;
	
	for (; *text && state != sep_lf1; text++)
		state = transtab[state][*(unsigned char*)text];
	*end = (char*) text;
	return state == sep_lf1;
}


#define ISWS(c) ((c)==' '||(c)=='\t')

#define REQERR_OK           0
#define REQERR_EXTRA_ARGS   1
#define REQERR_TOO_FEW_ARGS 2

static void
free_request_array(char **argv)
{
	int i;
	for (i = 0; i < 3 && argv[i]; i++)
		free(argv[i]);
}
		
static int
split_request(const char *request, char **argv)
{
	int i;

	argv[0] = argv[1] = argv[2] = NULL;
	for (i = 0; request[0]; i++) {
		size_t n;

		if (i == 3)
			return REQERR_EXTRA_ARGS;
		request += strspn(request, " \t");
		if (!request[0])
			break;

		n = strcspn(request, " \t");
		argv[i] = xmalloc(n + 1);
		memcpy(argv[i], request, n);
		argv[i][n] = 0;

		request += n;
	}
	if (i != 3)
		return REQERR_TOO_FEW_ARGS;
	return REQERR_OK;
}

static int
parse_http_request(struct joh_session *sess, char *ptr)
{
	char *p;
	char *argv[3];
	 struct http_decode_state *st = get_state(&sess->decstate);
	
	/* 1. Parse the request part */
	if (!find_eol(ptr, &p)) {
		error("%s: malformed HTTP request",
		      joh_sockaddr_str(sess->sa));
		return -1;
	}
	p[-2] = 0;

	if (split_request(ptr, argv)) {
		free_request_array(argv);
		error("%s: malformed HTTP request",
		      joh_sockaddr_str(sess->sa));
		return -1;
	}

	ptr = p;

	if (strncmp(argv[2], "HTTP/1.", 7)) {
		error("%s: malformed HTTP request",
		      joh_sockaddr_str(sess->sa));
		free_request_array(argv);
		return -1;
	}

	debug(JOH_DEBCAT_HTTP, 1,("query=%s", STR(argv[1])));
	if (strcasecmp(argv[0], "POST") == 0) {
		char *query;
		
		query = strchr(argv[1], '?');
		if (query)
			parse_post_query(sess->param, query + 1);
		sess->http_type = joh_http_poll;
	} else if (strcasecmp(argv[0], "CONNECT") == 0) {
		parse_connect_query(sess->param, argv[1]);
		sess->http_type = joh_http_connect;
	} else if (strcasecmp(argv[0], "GET") == 0) {
		sess->http_type = joh_http_get;
	} else {
		error("%s: unsupported request: %s",
		      joh_sockaddr_str(sess->sa), argv[0]);
		free_request_array(argv);
		return -1;
	}
	
	free_request_array(argv);

	debug(JOH_DEBCAT_HTTP, 1,
	      ("server=%s", STR(sess->param[joh_param_server].ptr)));
	debug(JOH_DEBCAT_HTTP, 1,
	      ("port=%s", STR(sess->param[joh_param_port].ptr)));
		       
	/* 2. Scan headers */
#define CONTENT_LENGTH_HEADER "Content-Length:"
#define CONTENT_LENGTH_HEADER_LEN (sizeof(CONTENT_LENGTH_HEADER)-1)
#define CONTENT_TYPE_HEADER "Content-Type:"
#define CONTENT_TYPE_HEADER_LEN (sizeof(CONTENT_TYPE_HEADER)-1)
#define CONTENT_TYPE "application/x-www-form-urlencoded"
#define CONTENT_TYPE_LEN (sizeof(CONTENT_TYPE)-1)
	
	for (; *ptr; find_eol(ptr, (char**)&ptr)) {
		if (strncasecmp(ptr, CONTENT_LENGTH_HEADER,
				CONTENT_LENGTH_HEADER_LEN) == 0) {
			char *p;

			st->content_length =
				strtoul(ptr + CONTENT_LENGTH_HEADER_LEN,
					&p, 10);
			for (;*p && ISWS(*p); p++);
			if (*p != '\r')
				return -1;
			ptr = p;
		} else if (strncasecmp(ptr, CONTENT_TYPE_HEADER,
				       CONTENT_TYPE_HEADER_LEN) == 0) {
			for (ptr += CONTENT_TYPE_HEADER_LEN; *ptr && ISWS(*ptr);
			     ptr++);
			if (strncmp(ptr, CONTENT_TYPE, CONTENT_TYPE_LEN))
				return -1;
		}
	}
	
	return 0;
}

int
joh_http_decode(struct joh_session *sess)
{
	char *iptr;
	size_t isize;
	struct http_decode_state *st = get_state(&sess->decstate);
	joh_cbuf_t ibuf = sess->ibuf;
	struct joh_param *param = sess->param;
	int rc;
	const char *id = joh_sockaddr_str(sess->sa);
	
	debug(JOH_DEBCAT_HTTP, 2, ("joh_http_decode called"));

	joh_cbuf_coalesce(ibuf);
	iptr = joh_cbuf_head(ibuf, &isize);

	if (st->state != sep_lf2) {
		st->state = find_separator(st->state,
					   (unsigned char*)iptr, isize,
					   st->off, &st->off);
		if (st->state != sep_lf2)
			return 1;
		debug(JOH_DEBCAT_HTTP, 2,
		      ("Found separator at %lu", (unsigned long) st->off));

		iptr[st->off-2] = 0;
		if (parse_http_request(sess, iptr))
			return -1;
	}
	if (st->content_length == 0)
		return 0;

	if (isize < st->off + st->content_length)
		return 1;

	rc = parse_content(param, iptr + st->off, st->content_length);
	free(st);
	sess->decstate = NULL;

	if (rc) {
		error("%s: invalid %s data",
		      id, http_type_str[sess->http_type]);
		return -1;
	}

	if (debug_level[JOH_DEBCAT_HTTP] >= 2) {
		int i;

		debug(JOH_DEBCAT_HTTP, 2,
		      ("%s: request %s",
		       id, http_type_str[sess->http_type]));
		for (i = 0; i < joh_param_max; i++)
			debug(JOH_DEBCAT_HTTP, 2,
			      ("%s: %s=%s", id, param_str[i],
			       STR(param[i].ptr)));
	}

	if (sess->http_type == joh_http_poll)
		return joh_param_check(sess);
	
	return 0;
}

/* Format HTTP reply */

static void
fmt_string(joh_cbuf_t tbuf, const char *str,
	   struct joh_param *param JOH_ARG_UNUSED)
{
	joh_cbuf_append(tbuf, str, strlen(str));
}

static void
fmt_server(joh_cbuf_t tbuf, const char *str,
	   struct joh_param *param JOH_ARG_UNUSED)
{
	joh_cbuf_append(tbuf, str, strlen(str));
	joh_cbuf_append_char(tbuf, ' ');
	fmt_string(tbuf, PACKAGE_NAME "/" PACKAGE_VERSION, param);
}

static void
fmt_cookie(joh_cbuf_t tbuf, const char *str, struct joh_param *param)
{
	if (param[joh_param_id].ptr) {
		joh_cbuf_append(tbuf, str, strlen(str));
		joh_cbuf_append(tbuf, " ID=", 4);
		joh_cbuf_append(tbuf, param[joh_param_id].ptr,
				param[joh_param_id].size);
	}
}

static void
fmt_date(joh_cbuf_t tbuf, const char *str,
	   struct joh_param *param JOH_ARG_UNUSED)
{
	char datebuf[32];
	time_t t = time(NULL);
	struct tm *tm;
	
	joh_cbuf_append(tbuf, str, strlen(str));
	joh_cbuf_append_char(tbuf, ' ');
	tm = gmtime(&t);
	strftime(datebuf, sizeof(datebuf), "%a, %d %b %Y %H:%M:%S GMT", tm);
	joh_cbuf_append(tbuf, datebuf, strlen(datebuf));
}

static void
fmt_content_length(joh_cbuf_t tbuf, const char *str, struct joh_param *param)
{
	size_t size = param[joh_param_payload].size;
	char nbuf[128];

	/* FIXME */
	snprintf(nbuf, sizeof(nbuf), "%lu", (unsigned long) size);
	joh_cbuf_append(tbuf, str, strlen(str));
	joh_cbuf_append_char(tbuf, ' ');
	joh_cbuf_append(tbuf, nbuf, strlen(nbuf));
}

static void
fmt_header_val(joh_cbuf_t tbuf, const char *str, struct joh_param *param)
{
	joh_cbuf_append(tbuf, str, strlen(str));
	joh_cbuf_append(tbuf, param[joh_param_payload].ptr,
			param[joh_param_payload].size);
}
	
struct reply_fmt {
	const char *text;
	void (*fun)(joh_cbuf_t tbuf, const char *str, struct joh_param *param);
};

struct reply_fmt poll_reply_tab[] = {
	{ "HTTP/1.1 200 OK", fmt_string },
	{ "Date:", fmt_date },
	{ "Server:", fmt_server },
	{ "Set-Cookie:", fmt_cookie },
	{ "Connection: Close", fmt_string },
	{ "Content-Type: text/xml", fmt_string },
	{ "Content-Length: ", fmt_content_length },
	{ NULL }
};

struct reply_fmt connect_reply_tab[] = {
	{ "HTTP/1.1 200 OK", fmt_string },
	{ "Date:", fmt_date }, 
	{ "Server:", fmt_server },
	{ NULL }
};

char default_error_reply[] = 
#include "404.c"
;

struct reply_fmt error_reply_tab[] = {
	{ "HTTP/1.1 404 Resource unavailable", fmt_string },
	{ "Date:", fmt_date },
	{ "Server:", fmt_server },
	{ "Content-Type: text/html", fmt_string },
	{ "Content-Length: ", fmt_content_length },
	{ NULL }
};

struct reply_fmt redirect_reply_tab[] = {
	{ "HTTP/1.1 303 Resource moved", fmt_string },
	{ "Date:", fmt_date },
	{ "Server:", fmt_server },
	{ "Location:", fmt_header_val },
	{ NULL }
};

static void
http_response(struct reply_fmt *fmt, struct joh_obuf *obuf,
	      struct joh_param *param)
{
	joh_cbuf_t tbuf = joh_cbuf_create();
	char *p;
	size_t size;
	
	for (; fmt->fun; fmt++) {
		fmt->fun(tbuf, fmt->text, param);
		joh_cbuf_append(tbuf, "\r\n", 2);
	}
	joh_cbuf_append(tbuf, "\r\n", 2);
	if (param[joh_param_payload].ptr) {
		joh_cbuf_append(tbuf, param[joh_param_payload].ptr,
				param[joh_param_payload].size);
	}
	size = joh_cbuf_size(tbuf);
	p = joh_cbuf_finish(tbuf);
	obuf->base = xmalloc(size);
	memcpy(obuf->base, p, size);
	obuf->base_alloc = 1;
	obuf->size = size;
	obuf->off = 0;
	joh_cbuf_free(tbuf);
}

int
joh_http_encode(struct joh_session *sess,
		struct joh_obuf *obuf, struct joh_param *param)
{
	struct reply_fmt *fmt;
		
	switch (sess->http_type) {
	case joh_http_poll:
		fmt = poll_reply_tab;
		break;

	case joh_http_connect:
		fmt = connect_reply_tab;
		break;

	case joh_http_get:
		if (!sess->sa->reply) {
			fmt = error_reply_tab;
			param[joh_param_payload].ptr = default_error_reply;
			param[joh_param_payload].size =
				strlen(default_error_reply);
		} else {
			param[joh_param_payload].ptr = sess->sa->reply->data;
			param[joh_param_payload].size =
				sess->sa->reply->length;
			
			fmt = sess->sa->reply->redirect ?
				redirect_reply_tab : error_reply_tab;
		}
		break;
		
	default:
		die(EX_SOFTWARE,
		    "%s: INTERNAL ERROR: invalid HTTP request type",
		    joh_sockaddr_str(sess->sa));
	}

	http_response(fmt, obuf, param);
	return 0;
}
