/* Session support for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

time_t joh_session_timeout_table[JOH_CONNTYPE_MAX] = {
	0,
	0,
	5*60,
	60
};

static struct joh_session *sess_head, *sess_tail;

void
joh_session_set_timeout(enum joh_conntype type, time_t timeout)
{
	joh_session_timeout_table[type] = timeout;
}

static void
session_attach_head(struct joh_session *sess)
{
	sess->next = sess_head;
	if (sess_head)
		sess_head->prev = sess;
	else
		sess_tail = sess;
	sess_head = sess;
}

void
joh_session_unlink(struct joh_session *sess)
{
	struct joh_session *p;

	p = sess->prev;
	if (p)
		p->next = sess->next;
	else
		sess_head = sess->next;
	p = sess->next;
	if (p)
		p->prev = sess->prev;
	else
		sess_tail = sess->prev;
	sess->next = sess->prev = NULL;
}

void
joh_session_promote(struct joh_session *sess)
{
	joh_session_unlink(sess);
	session_attach_head(sess);
	sess->last_used = time(NULL);
}

struct joh_session *
joh_session_create(int fd, enum joh_conntype type, struct joh_sockaddr *sa)
{
	struct joh_session *sess;

	debug(JOH_DEBCAT_SESS, 2,("%s FD %d", conn_type_str[type], fd));
	sess = xzalloc(sizeof(*sess));
	sess->fd = fd;
	sess->type = type;
	
	switch (type) {
	case conn_jabber:
	case conn_client:
		break;
	default:
		die(EX_SOFTWARE,
		    "internal error at %s:%d", __FILE__,__LINE__);
	}
	sess->ibuf = joh_cbuf_create();
	sess->sa = sa;
	session_attach_head(sess);
	sess->last_used = time(NULL);

	return sess;
}

static int sesstab_remove(const char *idstr);

void
joh_session_destroy(struct joh_session *sess)
{
	int i;

	if (!sess)
		return;
	if (sess->param[joh_param_id].ptr)
		debug(JOH_DEBCAT_SESS, 1,
		      ("destroying %s session for %s",
		       conn_type_str[sess->type],
		       sess->param[joh_param_id].ptr));
	else
		debug(JOH_DEBCAT_SESS, 1,
		      ("destroying %s session",
		       conn_type_str[sess->type]));
		
	if (sess->peer) {
		struct joh_session *js = sess->peer;
		js->peer = NULL;
		if (js->prev == NULL && js->next == NULL)
			joh_session_destroy(js);
	}
	debug(JOH_DEBCAT_SESS, 2,
	      ("closing %s FD %d", conn_type_str[sess->type], sess->fd));
	close(sess->fd);
	joh_rem_fd(sess->fd);
	joh_connection_register(sess->fd, conn_unused, NULL);	
	if (sess->type == conn_jabber)
		sesstab_remove(sess->param[joh_param_id].ptr);
	joh_session_unlink(sess);
	for (i = 0; i < joh_param_max; i++)
		free(sess->param[i].ptr);
	joh_cbuf_free(sess->ibuf);
	joh_obuf_free(&sess->obuf);
	joh_sockaddr_free(sess->sa);
	free(sess->decstate);
	free(sess);
}

/* Lookup table */

/* |hash_size| defines a sequence of symbol table sizes. These are prime
   numbers, the distance between each pair of them grows exponentially,
   starting from 64. Hardly someone will need more than 16411 symbols, and
   even if someone will, it is easy enough to add more numbers to the
   sequence. */

static unsigned int hash_size[] = {
    37,   101,  229,  487, 1009, 2039, 4091, 8191, 16411
};

/* |max_rehash| keeps the number of entries in |hash_size| table. */
static unsigned int max_rehash = sizeof (hash_size) / sizeof (hash_size[0]);

unsigned int hash_num;  /* Index to hash_size table */

struct joh_session **sesstab;

static unsigned
hash(const char *idstr, unsigned long hash_num)
{
	unsigned i;
	
	for (i = 0; *idstr; idstr++) {
		i <<= 1;
		i ^= *(unsigned char*) idstr;
	}
	return i % hash_size[hash_num];
}

static unsigned
insert_pos(const char *idstr)
{
	unsigned i;
	unsigned pos = hash(idstr, hash_num);
	
	for (i = pos; sesstab[i];) {
		if (++i >= hash_size[hash_num])
			i = 0;
		if (i == pos)
			/* FIXME: Error message? */
			abort();
	}
	return i;
}

static int
rehash()
{
	struct joh_session **new_tab;
	unsigned int i;
	unsigned int new_hash_num = hash_num + 1;
	
	if (new_hash_num >= max_rehash)
		return E2BIG;

	new_tab = xcalloc(hash_size[hash_num], sizeof(*new_tab));
	if (sesstab) {
		hash_num = new_hash_num;
		for (i = 0; i < hash_size[hash_num-1]; i++) {
			struct joh_session *elt = sesstab[i];
			if (elt) {
				unsigned n = insert_pos(elt->param[joh_param_id].ptr);
				new_tab[n] = elt;
			}
		}
		free(sesstab);
	}
	sesstab = new_tab;
	return 0;
}

static int
sesstab_remove(const char *idstr)
{
	unsigned int pos, i, j, r;
	struct joh_session *entry;

	if (!idstr)
		return ENOENT;
	pos = hash(idstr, hash_num);
	for (i = pos; entry = sesstab[i];) {
		if (strcmp(entry->param[joh_param_id].ptr, idstr) == 0)
			break;
		if (++i >= hash_size[hash_num])
			i = 0;
		if (i == pos)
			return ENOENT;
	}
	
	for (;;) {
		sesstab[i] = NULL;
		j = i;

		do {
			if (++i >= hash_size[hash_num])
				i = 0;
			if (!sesstab[i])
				return 0;
			r = hash(sesstab[i]->param[joh_param_id].ptr, hash_num);
		} while ((j < r && r <= i)
			 || (i < j && j < r) || (r <= i && i < j));
		sesstab[j] = sesstab[i];
	}
	return 0;
}

static int
sesstab_get_index(unsigned *idx, const char *idstr, int *install)
{
	unsigned i, pos;
	struct joh_session *elem;
  
	if (!sesstab) {
		if (install)
			rehash();
		else
			return 1;
	}

	pos = hash(idstr, hash_num);

	for (i = pos; elem = sesstab[i];) {
		if (strcmp(elem->param[joh_param_id].ptr, idstr) == 0) {
			if (install)
				*install = 0;
			*idx = i; 
			return 0;
		}
      
		if (++i >= hash_size[hash_num])
			i = 0;
		if (i == pos)
			break;
	}

	if (!install)
		return 1;
  
	if (!elem) {
		*install = 1;
		*idx = i;
		return 0;
	}

	rehash();

	return sesstab_get_index(idx, idstr, install);
}

struct joh_session *
joh_session_lookup(struct joh_session *sess)
{
	const char *id = sess->param[joh_param_id].ptr;
	unsigned i;
	struct joh_session *ent;
	struct joh_sockaddr *jsa;

	if (!id)
		return NULL;
	if (strcmp(id, "0") == 0) {
		int install;
		int fd;

		fd = new_jabber_connection(sess, &jsa);
		if (fd == -1)
			return NULL;

		ent = joh_session_create(fd, conn_jabber, jsa);
		ent->param[joh_param_id].ptr = xmalloc(sizeof(joh_sessid_t));
		ent->param[joh_param_id].size = sizeof(joh_sessid_t)-1;
		new_session_id(ent->param[joh_param_id].ptr);
		sess->param[joh_param_id].ptr =
			xrealloc(sess->param[joh_param_id].ptr,
				 sizeof(joh_sessid_t));
		strcpy(sess->param[joh_param_id].ptr,
		       ent->param[joh_param_id].ptr);
		sess->param[joh_param_id].size = ent->param[joh_param_id].size;

		debug(JOH_DEBCAT_SESS, 1,
		      ("created jabber session %s @ %s",
		       sess->param[joh_param_id].ptr,
		       joh_sockaddr_str(jsa)));
		       
		jsa->sess = ent;
		joh_connection_register(fd, conn_jabber, jsa);

		sesstab_get_index(&i, ent->param[joh_param_id].ptr, &install);
		if (install) {
			sesstab[i] = ent;
			return ent;
		} else {
			error("session id clash: %s",
			      ent->param[joh_param_id].ptr);
			joh_session_destroy(ent);
		}
	} else if (sesstab_get_index(&i, id, NULL) == 0) {
		ent = sesstab[i];
		joh_session_promote(ent);
		return ent;
	}
	error("session id %s not found", id);
	return NULL;
}

struct timeval *
joh_session_timeout(struct timeval *tv)
{
	long diff;

	while (sess_tail) {
		diff = time(NULL) - sess_tail->last_used;
		if (diff < joh_session_timeout_table[sess_tail->type])
			break;
		joh_session_destroy(sess_tail);
	}
	
	if (sess_tail) {
		tv->tv_sec = joh_session_timeout_table[sess_tail->type] - diff;
		tv->tv_usec = 0;
	} else
		tv = NULL;
	return tv;
}

static struct {
	int (*setup)(enum joh_client_dir, struct joh_session *);
	int (*flush)(enum joh_client_dir, struct joh_session *);
} session_handler[]  = {
	{ NULL, NULL },
	{ joh_http_poll_setup, joh_http_poll_flush },
	{ NULL, joh_http_connect_flush },
	{ NULL, joh_http_transparent_flush },
	{ NULL, joh_http_get_flush }
};

int
joh_client_session_setup(enum joh_client_dir dir, struct joh_session *sess)
{
	unsigned n;

	n = sess->http_type;
	if (n > sizeof(session_handler) / sizeof(session_handler[0]))
		die(EX_SOFTWARE,
		    "%s: INTERNAL ERROR: wrong HTTP type during setup",
		    joh_sockaddr_str(sess->sa));
	if (!session_handler[n].setup)
		return 0;
	return session_handler[n].setup(dir, sess);
}

int
joh_client_session_flush(enum joh_client_dir dir, struct joh_session *sess)
{
	unsigned n;

	n = sess->http_type;
	if (n > sizeof(session_handler) / sizeof(session_handler[0]))
		die(EX_SOFTWARE,
		    "%s: INTERNAL ERROR: wrong HTTP during flush",
		    joh_sockaddr_str(sess->sa));
	if (!session_handler[n].flush)
		return 0;
	return session_handler[n].flush(dir, sess);
}

