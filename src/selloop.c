/* Select-based JOH loop.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

static int
_netio_unused(int fd, struct joh_sockaddr *sa, void *data)
{
	die(EX_SOFTWARE, "internal error: unhandled descriptor %d",
	    fd);
}

static int
_netio_master(int fd, struct joh_sockaddr *sa, void *data)
{
	int sd;
	fd_set *fds = data;
	union {
		struct sockaddr_in s_in;
		struct sockaddr_in6 s_in6;
		struct sockaddr_un s_un;
		struct sockaddr s_sa;
	} s;
	socklen_t len = sizeof s;
	struct joh_sockaddr *jsa;
	
	if (!FD_ISSET(fd, &fds[FDSET_RD]))
		return 0;
	sd = accept(fd, (struct sockaddr *) &s, &len);
	if (sd < 0) {
		error("accept(%s): %s",
		      joh_sockaddr_str(sa), strerror(errno));
		return 0; /* FIXME: perhaps 1? */
	}

	if (!connection_allowed(sd, sa->srvname)) {
		char *addr_string = sockaddr_str(&s.s_sa, len);
		error("access from %s blocked by TCP wrappers, service %s",
		      addr_string, sa->srvname);
		free(addr_string);
		close(sd);
		return 0;
	}

	jsa = joh_sockaddr_new(len);
	memcpy(jsa->addr, &s, len);
	joh_add_fd(sd, FDMASK_RD);
	jsa->sess = joh_session_create(sd, conn_client, jsa);
	jsa->class = sa->class;
	jsa->srvname = sa->srvname;
	jsa->reply = sa->reply;
	
	debug(JOH_DEBCAT_SESS, 1,
	      ("created client session @ %s", joh_sockaddr_str(jsa)));
	joh_connection_register(sd, conn_client, jsa);
	
	return 0;
}

static int
_netio_jabber(int fd, struct joh_sockaddr *sa, void *data)
{
	fd_set *fds = data;
	struct joh_session *sess = sa->sess;
	ssize_t rc;
	
	if (FD_ISSET(fd, &fds[FDSET_WR]) &&
	    sess->obuf.off < sess->obuf.size) {
		rc = send(fd, sess->obuf.base + sess->obuf.off,
			  sess->obuf.size - sess->obuf.off, 0);
		if (rc == -1) {
			error("%s: write error on %s: %s",
			      conn_type_str[sess->type],
			      joh_sockaddr_str(sa), strerror(errno));
			joh_session_destroy(sess);
			return 0;
		}
		if (debug_level[JOH_DEBCAT_PROTO] >= 10) {
			char *pfx[4] = { "Jabber", NULL, "S", NULL };
			pfx[1] = (char*) joh_sockaddr_str(sa);
			joh_logdump(pfx, sess->obuf.base + sess->obuf.off, rc);
		}
		sess->obuf.off += rc;
		if (sess->obuf.off == sess->obuf.size) {
			joh_chg_fd(sess->fd, FDMASK_RD);
			joh_obuf_free(&sess->obuf);
			return 0;
		}
	}
	
	if (FD_ISSET(fd, &fds[FDSET_RD])) {
		char iobuf[IOBUFSIZE];
		
		rc = recv(fd, iobuf, sizeof(iobuf), 0);
		if (rc == 0) {
			debug(JOH_DEBCAT_SESS, 1,
			      ("Jabber@%s: got EOF, closing",
			       joh_sockaddr_str(sa)));
			joh_session_destroy(sess);
			return 0;
		} 
		if (rc == -1) {
			error("%s: read error on %s: %s",
			      conn_type_str[sess->type],
			      joh_sockaddr_str(sa), strerror(errno));
			joh_session_destroy(sess);
			return 0;
		}
		if (debug_level[JOH_DEBCAT_PROTO] >= 10) {
			char *pfx[4] = { "Jabber", NULL, "C", NULL };
			pfx[1] = (char*) joh_sockaddr_str(sa);
			joh_logdump(pfx, iobuf, rc);
		}
		
		joh_cbuf_append(sess->ibuf, iobuf, rc);
		if (sess->peer)
			joh_chg_fd(sess->peer->fd, FDMASK_RD|FDMASK_WR);
	}
	return 0;
}

static int
_netio_client(int fd, struct joh_sockaddr *sa, void *data)
{
	fd_set *fds = data;
	struct joh_session *sess = sa->sess;
	ssize_t rc;
	
	if (FD_ISSET(fd, &fds[FDSET_RD])) {
		char iobuf[IOBUFSIZE];

		rc = recv(fd, iobuf, sizeof(iobuf), 0);
		if (rc == 0) {
			debug(JOH_DEBCAT_PROTO, 1,
			      ("%s/%s@%s: got EOF, closing",
				  conn_type_str[sess->type],
				  client_class_str[sa->class],
				  joh_sockaddr_str(sa)));
			joh_session_destroy(sess);
			return 0;
		} 
		if (rc == -1) {
			error("%s/%s: read error on %s: %s",
			      conn_type_str[sess->type],
			      client_class_str[sa->class],
			      joh_sockaddr_str(sa), strerror(errno));
			joh_session_destroy(sess);
			return 0;
		}
		if (debug_level[JOH_DEBCAT_PROTO] >= 10) {
			char *pfx[4] = { NULL, NULL, "C", NULL };
			pfx[0] = client_class_str[sa->class];
			pfx[1] = (char*) joh_sockaddr_str(sa);
			joh_logdump(pfx, iobuf, rc);
		}
		joh_cbuf_append(sess->ibuf, iobuf, rc);
		rc = joh_data_decode(sess);
		if (rc == 0) {			
			joh_cbuf_clear(sess->ibuf);
			rc = joh_client_session_flush(joh_client_in, sess);
		}
		if (rc < 0) {
			error("%s/%s: protocol error on %s: %s",
			      conn_type_str[sess->type],
			      client_class_str[sa->class],
			      joh_sockaddr_str(sa), strerror(errno));
			joh_session_destroy(sess);
		}
		return 0;
	}
	
	if (FD_ISSET(fd, &fds[FDSET_WR])) {
		if (!sess->obuf.base) {
			int rc;
			struct joh_param param[joh_param_max];

			memset(param, 0, sizeof(param));
			param[joh_param_id] = sess->param[joh_param_id];
			if (sess->peer) {
				size_t size;
				
				joh_client_session_setup(joh_client_out, sess);
				size = joh_cbuf_size(sess->peer->ibuf);
				if (size) {
					param[joh_param_payload].ptr =
						joh_cbuf_finish(sess->peer->ibuf);
					param[joh_param_payload].size = size;
					rc = joh_data_encode(sa,
							     &sess->obuf,
							     param);
					joh_cbuf_clear(sess->peer->ibuf);
				} else {
					rc = joh_data_encode(sa,
							     &sess->obuf,
							     param);
				}
			} else {
				rc = joh_data_encode(sa,
						     &sess->obuf,
						     param);
			}
			if (rc < 0) {
				joh_session_destroy(sess);
				return 0;
			}
		}
		rc = send(fd, sess->obuf.base + sess->obuf.off,
			  sess->obuf.size - sess->obuf.off, 0);
		if (rc == -1) {
			error("%s/%s: write error on %s: %s",
			      conn_type_str[sess->type],
			      client_class_str[sa->class],
			      joh_sockaddr_str(sa), strerror(errno));
			joh_session_destroy(sess);
		} else {
			if (debug_level[JOH_DEBCAT_PROTO] >= 10) {
				char *pfx[4] = { NULL, NULL, "S", NULL };
				pfx[0] = client_class_str[sa->class];
				pfx[1] = (char*) joh_sockaddr_str(sa);
				joh_logdump(pfx,
					    sess->obuf.base + sess->obuf.off,
					    rc);
			}
			sess->obuf.off += rc;
			if (sess->obuf.off == sess->obuf.size) {
				joh_client_session_flush(joh_client_out, sess);
			}
		}
	}

	return 0;
}

static int (*netio_handler[])(int, struct joh_sockaddr *, void *) = {
	_netio_unused,
	_netio_master,
	_netio_jabber,
	_netio_client
};

int
netio_itr(int fd, enum joh_conntype type, struct joh_sockaddr *sa, void *data)
{
	return netio_handler[type](fd, sa, data);
}

static int got_signal;

static RETSIGTYPE
sighan(int sig)
{
	got_signal = sig;
}

static void
set_signals(RETSIGTYPE (*handler)(int), int *sigv, int sigc)
{
	int i;
	struct sigaction act;

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	for (i = 0; i < sigc; i++)
		sigaddset(&act.sa_mask, sigv[i]);

	for (i = 0; i < sigc; i++) {
		act.sa_handler = handler;
		sigaction(sigv[i], &act, NULL);
	}
}

int
joh_loop()
{
	int rc;
	fd_set fds[_FDSET_MAX];
	static int sigtab[] = { SIGPIPE, /*SIGABRT,*/ SIGINT, SIGQUIT,
				SIGTERM, SIGHUP };
	
	set_signals(sighan, sigtab, sizeof(sigtab)/sizeof(sigtab[0]));

	while (!got_signal) {
		struct timeval tv, *tvp;

		tvp = joh_session_timeout(&tv);
		memcpy(fds, joh_fd_set, sizeof(fds));

		rc = select(joh_max_fd + 1, &fds[FDSET_RD], &fds[FDSET_WR],
			    NULL, tvp);
		if (rc == -1) {
			if (errno == EINTR)
				continue;
			die(EX_OSERR, "select: %s", strerror(errno));
		}
		joh_connection_iterate(netio_itr, fds, 0);
	}
	set_signals(SIG_DFL, sigtab, sizeof(sigtab)/sizeof(sigtab[0]));
	return got_signal;
}
