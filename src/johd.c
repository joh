/* johd: Main Jabber-Over-HTTP daemon.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>

char *conffile;
char *pidfile = NULL;
int preprocess_only = 0;
int lint_mode = 0;

struct joh_sockaddr *listen_addrs;
struct joh_sockaddr *jabber_addr;

int daemon_option = 0;
char *user_name = NULL;
struct joh_sockaddr_hints
        default_listen_hints = { NULL,
				 JOH_HINT_BIND|JOH_HINT_SERVICE|JOH_HINT_CLASS,
				 DEFAULT_PORT_STRING, client_cgi },
	listen_hints = { NULL,
			 JOH_HINT_BIND|JOH_HINT_SERVICE|JOH_HINT_CLASS,
			 DEFAULT_PORT_STRING, client_cgi },
	jabber_hints = { NULL,
			 JOH_HINT_SERVICE, JABBER_PORT_STRING };

void
joh_check_pidfile(const char *name)
{
	unsigned long pid;
	FILE *fp = fopen(name, "r");
	if (!fp) {
		if (errno == ENOENT)
			return;
		error("cannot open pidfile `%s': %s",
		      name, strerror(errno));
		exit(EX_TEMPFAIL);
	}
	if (fscanf(fp, "%lu", &pid) != 1) {
		error("cannot read pidfile `%s'", name);
	} else {
		if (kill(pid, 0) == 0) {
			error("%s appears to run with pid %lu; "
			      "if it does not, remove `%s' and retry",
				program_name,
				pid,
			      name);
			exit(EX_USAGE);
		}
	}
	fclose(fp);
	if (unlink(name)) {
		error("cannot unlink pidfile `%s': %s",
		      name, strerror(errno));
		exit(EX_USAGE);
	}
}

void
joh_remove_pidfile(void)
{
	unlink(pidfile);
}

void
joh_write_pidfile(const char *name)
{
	FILE *fp = fopen(name, "w");
	if (!fp) {
		error("cannot create pidfile `%s': %s",
		      name, strerror(errno));
		exit(EX_TEMPFAIL);
	}
	fprintf(fp, "%lu\n", (unsigned long) getpid());
	fclose(fp);
	atexit(joh_remove_pidfile);
}

static void
setuserpriv(const char *user_name)
{
	struct passwd *pw;
	
	if (!user_name)
		return;
	pw = getpwnam(user_name);
	if (!pw)
		die(EX_USAGE, "no such user: %s", user_name);
	debug(JOH_DEBCAT_MAIN, 2,
	      ("switching to privileges of %s (%lu:%lu)",
	       user_name, (unsigned long)pw->pw_uid,
	       (unsigned long)pw->pw_gid));
	if (setgroups(1, &pw->pw_gid))
		die(EX_UNAVAILABLE,
		    "setgroups failed: %s", strerror(errno));
	if (setgid(pw->pw_uid))
		die(EX_UNAVAILABLE,
		    "setgid(%lu) failed: %s",
		    (unsigned long) pw->pw_gid, strerror(errno));
	if (setuid(pw->pw_uid))
		die(EX_UNAVAILABLE,
		    "setuid(%lu) failed: %s",
		    (unsigned long) pw->pw_uid, strerror(errno));
}


#include "cmdline.h"

int
main(int argc, char **argv)
{
	struct grecs_node *tree = NULL;
	
	set_program_name(argv[0]);
	config_init();
	parse_options(argc, argv);

	grecs_gram_trace(debug_level[JOH_DEBCAT_CFGRAM]);
	grecs_lex_trace(debug_level[JOH_DEBCAT_CFLEX]);

	if (!conffile) {
		if (preprocess_only || lint_mode ||
		    access(SYSCONFDIR "/johd.conf", F_OK) == 0)
			conffile = SYSCONFDIR "/johd.conf";
	}
			
	if (conffile) {
		if (preprocess_only)
			exit(grecs_preproc_run(conffile, grecs_preprocessor) ?
			     EX_CONFIG : 0);

		tree = grecs_parse(conffile);
		if (!tree)
			exit(EX_CONFIG);
	}
	if (!tree)
		tree = cmdline_tree;
	else if (cmdline_tree) {
		grecs_tree_join(tree, cmdline_tree);
		grecs_tree_free(cmdline_tree);
	}
	if (tree)
		config_finish(tree);
	if (lint_mode)
		exit(grecs_error_count ? EX_CONFIG : 0);

	if (pidfile)
		joh_check_pidfile(pidfile);
	
	if (daemon_option) {
		log_to_stderr = 0;
		if (daemon(0, 0))
			die(EX_UNAVAILABLE,
			    "cannot switch to background: %s",
			    strerror(errno));
	} else
		log_to_stderr = isatty(0);
	
	joh_initlog();
	debug(JOH_DEBCAT_MAIN, 1, ("johd %s startup", PACKAGE_VERSION));
	if (!listen_addrs)
		listen_addrs = parse_sockaddr("inet://", &listen_hints);
	if (!jabber_addr) {
		/* FIXME: Use SRV record, if available. Ditto for -l. */
		jabber_addr = parse_sockaddr(JABBER_SERVER, &jabber_hints);
		if (jabber_addr->next) {
			joh_sockaddr_free(jabber_addr->next);
			jabber_addr->next = NULL;
		}
	}

	joh_conntab_init();
	joh_listen_socket_setup(listen_addrs);
	setuserpriv(user_name);
	if (pidfile)
		joh_write_pidfile(pidfile);
	joh_loop();
	joh_listen_socket_cleanup();
	debug(JOH_DEBCAT_MAIN, 1, ("johd %s terminated", PACKAGE_VERSION));
	
	return EX_OK;
}
