/* Network-specific functions for johd.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify it 
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"
#include <ctype.h>
#include <string.h>

/*
inet://
inet6://
unix://
*/

static int 
good_ipv4(const char *addr)
{
        int dot_count = 0;
        int digit_count = 0;

	for (; *addr; addr++) {
		if (!isascii(*addr))
			return 0;
                if (*addr == '.') {
                        if (++dot_count > 3)
                                break;
                        digit_count = 0;
                } else if (!(isdigit(*addr) && ++digit_count <= 3))
                        return 0;
        }

        return (dot_count == 3);
}

static int 
good_ipv6(const char *addr)
{
        int col_count = 0; /* Number of colons */
	int dcol = 0;      /* Did we encounter a double-colon? */
	int dig_count = 0; /* Number of digits in the last group */
	
	for (; *addr; addr++) {
		if (!isascii(*addr))
			return 0;
		else if (isxdigit(*addr)) {
			if (++dig_count > 4)
				return 0;
		} else if (*addr == ':') {
			if (col_count && dig_count == 0 && ++dcol > 1)
				return 0;
			if (++col_count > 7)
				return 0;
			dig_count = 0;
		} else
			return 0;
	}

	return (col_count == 7 || dcol);
}

static int 
good_ipaddr(const char *addr)
{
	if (strchr(addr, '.'))
		return good_ipv4(addr);
	else if (strchr(addr, ':'))
		return good_ipv6(addr);
	return 0;
}

static int 
is_numstr(const char *p)
{
	if (!*p)
		return 0;
	for (; *p && isascii(*p) && isdigit(*p);p++)
		;
	return *p == 0;
}

struct joh_sockaddr *
joh_sockaddr_new(socklen_t len)
{
	struct joh_sockaddr *p = xzalloc(sizeof(struct joh_sockaddr));
	p->addr = xzalloc(len);
	p->addrlen = len;
	return p;
}

struct joh_sockaddr *
joh_sockaddr_copy(struct joh_sockaddr *src)
{
	struct joh_sockaddr *p = joh_sockaddr_new(src->addrlen);
	memcpy(p->addr, src->addr, src->addrlen);
	return p;
}
	
struct joh_sockaddr *
joh_sockaddr_append(struct joh_sockaddr *a, struct joh_sockaddr *b)
{
	struct joh_sockaddr *t;
	
	if (!a) 
		return b;
	
	for (t = a; t->next; t = t->next);
	t->next = b;
	b->prev = t;
	return a;
}

void
joh_sockaddr_free(struct joh_sockaddr *sp)
{
	while (sp) {
		struct joh_sockaddr *next = sp->next;
		free(sp->str);
		free(sp->addr);
		free(sp);
		sp = next;
	}
}

static struct joh_sockaddr *
parse_unix(const char *arg, const char *addrstr, struct joh_sockaddr_hints *jh)
{
	struct sockaddr_un *s_un;
	size_t slen = strlen(addrstr);
	struct joh_sockaddr *sp;

	if (slen >= sizeof s_un->sun_path) {
		die(EX_CONFIG, "socket path name too long: %s", arg);
		return NULL;
	}
	
	sp = joh_sockaddr_new(sizeof(s_un[0]));
	s_un = (struct sockaddr_un *) sp->addr;
	s_un->sun_family = AF_UNIX;
	strcpy(s_un->sun_path, addrstr);
	if (jh->flags & JOH_HINT_CLASS)
		sp->class = jh->class;
	sp->srvname = NULL; /* Not used for UNIX sockets */
	return sp;
}

static struct joh_sockaddr *
parse_inet(int family, const char *arg, const char *addrstr,
	   struct joh_sockaddr_hints *jh)
{
	int rc;
	struct joh_sockaddr *head = NULL, *tail = NULL;
	struct addrinfo hints;
	struct addrinfo *res, *ap;
	const char *node = NULL;
	char *nodebuf = NULL;
	const char *service = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = family;
	hints.ai_socktype = SOCK_STREAM;

	if ((family == AF_INET6 || family == AF_UNSPEC)
	    && addrstr[0] == '[') {
		char *p = strchr(addrstr + 1, ']');
		if (p && p > addrstr + 1) {
			size_t len;
			
			addrstr++;
			len = p - addrstr;
			nodebuf = xmalloc(len + 1);
			memcpy(nodebuf, addrstr, len);
			nodebuf[len] = 0;
			node = nodebuf;
			service = p + 1;
			family = AF_INET6;
		} else
			service = strchr(addrstr, ':');
	} else
		service = strrchr(addrstr, ':');

	if (service && *service) {
		if (*service != ':')
			die(EX_CONFIG, "%s: garbage near %s", arg, service);
		service++;
	}
	
	if (!node) {
		if (service) {
			size_t len = service - addrstr - 1;

			if (len == 0)
				node = NULL;
			else {
				nodebuf = xmalloc(len + 1);
				memcpy(nodebuf, addrstr, len);
				nodebuf[len] = 0;
				node = nodebuf;
			}
		} else {
			if (good_ipaddr(addrstr))
				node = addrstr;
			else if (is_numstr(addrstr)) {
				service = addrstr;
				hints.ai_flags |= AI_NUMERICSERV;
			}
		}
	}
	
	if (!service || !*service) {
		if (jh->flags & JOH_HINT_SERVICE) {
			if (!node && addrstr[0])
				node = addrstr;
			service = jh->service;
			hints.ai_flags |= AI_NUMERICSERV;
		} else
			die(EX_CONFIG, "service not specified: %s", arg);
	}
	
	if (!node) {
		if (jh->flags & JOH_HINT_BIND)
			hints.ai_flags |= AI_PASSIVE;
	}
	
	rc = getaddrinfo(node, service, &hints, &res);
	free(nodebuf);
	switch (rc) {
	case 0:
		break;
	case EAI_SYSTEM:
		die(EX_CONFIG, "%s: cannot parse address: %s",
		    arg, strerror(errno));
		break;
	case EAI_BADFLAGS:
	case EAI_SOCKTYPE:
		die(EX_SOFTWARE, "%s:%d: internal error converting %s",
		    __FILE__,__LINE__,arg);
		break;
	case EAI_MEMORY:
		xalloc_die();
	default:
		die(EX_CONFIG, "%s: %s", arg, gai_strerror(rc));
	}

	for (ap = res; ap; ap = ap->ai_next) {
		if (family == AF_UNSPEC || ap->ai_addr->sa_family == family) {
			struct joh_sockaddr *sp =
				joh_sockaddr_new(ap->ai_addrlen);
			memcpy(sp->addr, ap->ai_addr, ap->ai_addrlen);
			if (jh->flags & JOH_HINT_CLASS)
				sp->class = jh->class;
			if (jh->flags & JOH_HINT_SRVNAME)
				sp->srvname = jh->srvname;
			else
				sp->srvname = program_name;
			if (jh->flags & JOH_HINT_REPLY)
				sp->reply = jh->reply;
			sp->prev = tail;
			if (tail)
				tail->next = sp;
			else
				head = sp;
			tail = sp;
		}
	}
	freeaddrinfo(res);
	return head;
}

static struct joh_sockaddr *
parse_inet4(const char *arg, const char *addrstr,
	    struct joh_sockaddr_hints *jh)
{
	return parse_inet(AF_INET, arg, addrstr, jh);
}

static struct joh_sockaddr *
parse_inet6(const char *arg, const char *addrstr,
	    struct joh_sockaddr_hints *jh)
{
	return parse_inet(AF_INET6, arg, addrstr, jh);
}

struct schemetab {
	const char *scheme;
	size_t len;
	struct joh_sockaddr *(*parser)(const char *arg, const char *addr,
				       struct joh_sockaddr_hints *jh);
};

struct schemetab schemetab[] = {
	{ "inet", 4, parse_inet4 },
	{ "inet4", 5, parse_inet4 },
	{ "inet6", 5, parse_inet6 },
	{ "unix", 4, parse_unix },
	{ NULL }
};

struct joh_sockaddr *
parse_sockaddr(const char *arg, struct joh_sockaddr_hints *jh)
{
	char *p;
	struct joh_sockaddr_hints jhints;

	if (!jh) {
		memset(&jhints, 0, sizeof(jhints));
		jh = &jhints;
	}
	
	p = strchr(arg, ':');
	if (p && p > arg && p[1] == '/' && p[2] == '/') {
		size_t len = p - arg;
		struct schemetab *sp;

		for (sp = schemetab; sp->scheme; sp++)
			if (len == sp->len &&
			    memcmp(arg, sp->scheme, len) == 0)
				return sp->parser(arg, p + 3, jh);
		die(EX_USAGE,
		    "unknown or unsupported scheme: %s", arg);
	}

	if (arg[0] == '/')
		return parse_unix(arg, arg, jh);
	else if (strlen(arg) > 5 && memcmp(arg, "unix:", 5) == 0) {
		if (arg[5] != '/')
			die(EX_USAGE,
			    "%s: UNIX socket must be an absolute file name",
			    arg);
		return parse_unix(arg, arg + 5, jh);
	}
	
	return parse_inet(AF_UNSPEC, arg, arg, jh);
}

#define S_UN_NAME(sa, salen) \
	((salen < offsetof(struct sockaddr_un,sun_path)) ?	\
	  "" : (sa)->sun_path)

char *
sockaddr_str(struct sockaddr *sa, socklen_t salen)
{
	char buf[512];

	switch (sa->sa_family) {
	case AF_INET:
	case AF_INET6: {
		char host[NI_MAXHOST];
		char srv[NI_MAXSERV];
		if (getnameinfo(sa, salen,
				host, sizeof(host), srv, sizeof(srv),
				NI_NUMERICHOST|NI_NUMERICSERV) == 0)
			snprintf(buf, sizeof buf, "%s://%s:%s",
				 sa->sa_family == AF_INET ?
				   "inet" : "inet6",
				 host, srv);
		else
			snprintf(buf, sizeof buf, "%s://[getnameinfo failed]",
				 sa->sa_family == AF_INET ?
				   "inet" : "inet6");
		break;
	}

	case AF_UNIX: {
		struct sockaddr_un *s_un = (struct sockaddr_un *)sa;
		if (S_UN_NAME(s_un, salen)[0] == 0)
			snprintf(buf, sizeof buf, "unix://[anonymous socket]");
		else
			snprintf(buf, sizeof buf, "unix://%s", s_un->sun_path);
		break;
	}

	default:
		snprintf(buf, sizeof buf, "family:%d", sa->sa_family);
	}
	return xstrdup (buf);
}

const char *
joh_sockaddr_str(struct joh_sockaddr *sa)
{
	if (!sa->str)
		sa->str = sockaddr_str(sa->addr, sa->addrlen);
	return sa->str;
}

			

