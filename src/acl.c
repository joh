/* TCP-wrapper-based ACLs.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"
#include <tcpd.h>

int
connection_allowed(int fd, const char *srvname)
{
	struct request_info req;

	request_init(&req,
		     RQ_DAEMON, srvname,
		     RQ_FILE, fd, NULL);
	fromhost(&req);
	return hosts_access(&req);
}

#define SRVSUF "/jabber"

int
destination_allowed(const char *srvname,
		    struct sockaddr *sa_clt, struct sockaddr *sa_srv)
{
	int rc;
	struct request_info req;
	char *daemon;
	size_t len;

	len = strlen(srvname) + sizeof(SRVSUF);
	daemon = xmalloc(len);
	strcpy(daemon, srvname);
	strcat(daemon, SRVSUF);
	request_init(&req,
		     RQ_DAEMON, daemon,
		     RQ_CLIENT_SIN, sa_clt,
		     RQ_SERVER_SIN, sa_srv,
		     NULL);
	fromhost(&req);
	rc = hosts_access(&req);
	free(daemon);
	return rc;
}
