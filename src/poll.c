/* HTTP Polling support for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

int
joh_http_poll_setup(enum joh_client_dir dir, struct joh_session *sess)
{
	switch (dir) {
	case joh_client_none:
		break;
	case joh_client_in:
		break;
	case joh_client_out:
		joh_chg_fd(sess->peer->fd, FDMASK_RD);
	}
	return 0;
}

int
joh_http_poll_flush(enum joh_client_dir dir, struct joh_session *sess)
{
	struct joh_sockaddr *sa = sess->sa;
	struct joh_session *js;

	switch (dir) {
	case joh_client_none:
		break;
		
	case joh_client_in:
		joh_chg_fd(sess->fd, FDMASK_RD|FDMASK_WR);

		js = joh_session_lookup(sess);
		if (!js) {
			strcpy(sess->param[joh_param_id].ptr, "0:0");
			sess->param[joh_param_id].size = 3;
			return 0;
		}

		if (!joh_validate_key(sess, js)) {
			error("%s/%s: connection from %s has invalid key",
			      conn_type_str[sess->type],
			      client_class_str[sa->class],
			      joh_sockaddr_str(sa));
			/* XEP-0025:
			   3.1.4 Key Sequence Error
			   
			   Server returns ID=-3:0
			*/
			strcpy(sess->param[joh_param_id].ptr, "-3:0");
			sess->param[joh_param_id].size = 4;
			return 0;
		}				 

		if (sess->param[joh_param_newkey].ptr) {
			free(js->param[joh_param_key].ptr);
			js->param[joh_param_key].ptr =
				sess->param[joh_param_newkey].ptr;
			js->param[joh_param_key].size =
				sess->param[joh_param_newkey].size;
			sess->param[joh_param_newkey].ptr = NULL;
			sess->param[joh_param_newkey].size = 0;
		}
		
		sess->peer = js;
		js->peer = sess;
		if (sess->param[joh_param_payload].size) {
			joh_chg_fd(js->fd, FDMASK_RD|FDMASK_WR);
			js->obuf.base = sess->param[joh_param_payload].ptr;
			js->obuf.base_alloc = 0;
			js->obuf.size = sess->param[joh_param_payload].size;
			js->obuf.off = 0;
		} else
			joh_chg_fd(js->fd, FDMASK_RD);
		break;
		
	case joh_client_out:
		js = sess->peer;
		if (js)
			joh_chg_fd(js->fd, FDMASK_RD);
		joh_session_destroy(sess);
	}
	return 0;
}

