/* JOH data interpreter.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

struct interpreter {
	int (*decode) (struct joh_session *);
	int (*encode) (struct joh_session *, struct joh_obuf *,
		       struct joh_param *);

};

static int
decode_error(struct joh_session *sess)
{
	const char *id = joh_sockaddr_str(sess->sa);
	char *pfx[2] = { (char*) id, NULL };

	error("%s: INTERNAL ERROR: wrong client class while decoding", id);
	joh_logdump(pfx, joh_cbuf_finish(sess->ibuf),
		    joh_cbuf_size(sess->ibuf));
	return -1;
}

static int
encode_error(struct joh_session *sess JOH_ARG_UNUSED,
	     struct joh_obuf *buf JOH_ARG_UNUSED,
	     struct joh_param *param_vals JOH_ARG_UNUSED)
{
	error("INTERNAL ERROR: wrong client class while encoding");
	return -1;
}

int
joh_param_check(struct joh_session *sess)
{
	struct joh_sockaddr *sa = sess->sa;
	
	if (!sess->param[joh_param_id].ptr) {
		error("no ID in %s packet from %s",
		      client_class_str[sa->class],
		      joh_sockaddr_str(sa));
		return -1;
	}
	if (!sess->param[joh_param_key].ptr) {
		error("no key in %s packet from %s",
		      client_class_str[sa->class],
		      joh_sockaddr_str(sa));
		return -1;
	}
	return 0;
}

static struct interpreter interpreter[] = {
	{ decode_error, encode_error },
	{ joh_pdu_decode, joh_pdu_encode },
	{ joh_http_decode, joh_http_encode },
	{ joh_transparent_decode, joh_transparent_encode }
};

int
joh_data_decode(struct joh_session *sess)
{
	struct joh_sockaddr *sa = sess->sa;
	if (!interpreter[sa->class].decode)
		return 0;
	return interpreter[sa->class].decode(sess);
}

int
joh_data_encode(struct joh_sockaddr *sa,
		struct joh_obuf *buf,
		struct joh_param *param)
{
	if (!interpreter[sa->class].encode)
		return 0;
	return interpreter[sa->class].encode(sa->sess, buf, param);
}
