/* Diagnostic functions for JOH.
   Copyright (C) 2011, 2013 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"
#include <ctype.h>
#include <string.h>
#include <syslog.h>

const char *program_name;
int debug_level[_JOH_DEBCAT_MAX];
int log_to_stderr = -1;
const char *syslog_tag = NULL;
int syslog_include_prio = 0;
int syslog_facility = LOG_DAEMON;
int syslog_source_info = 0;

int source_info_option;

void
set_program_name(const char *arg)
{
	const char *p = strrchr(arg, '/');
	if (p)
		program_name = p + 1;
	else
		program_name = arg;
}


struct debug_trans {
	const char *name;
	size_t length;
	int cat;
};

static struct debug_trans debug_trans[] = {
#define S(s) #s, sizeof(#s)-1
	{ S(main),   JOH_DEBCAT_MAIN },
	{ S(http),   JOH_DEBCAT_HTTP },
	{ S(proto),  JOH_DEBCAT_PROTO },
	{ S(sess),   JOH_DEBCAT_SESS },
	{ S(cfgram), JOH_DEBCAT_CFGRAM },
	{ S(cflex),  JOH_DEBCAT_CFLEX },
	{ NULL }
};

int
parse_debug_level(const char *arg)
{
	unsigned long cat, lev;
	char *p = NULL;

	if (isascii(*arg) && isdigit(*arg)) {
		lev = strtoul(arg, &p, 10);
		if (*p == 0) {
			for (cat = 0; cat < _JOH_DEBCAT_MAX; cat++)
				debug_level[cat] = lev;
			return 0;
		} else if (*p != '.')
			return -1;
		cat = lev;
		if (cat > _JOH_DEBCAT_MAX)
			return -1;
		lev = 0;
	} else {
		size_t len = strcspn(arg, ".");
		struct debug_trans *dp;

		for (dp = debug_trans; dp->name; dp++)
			if (dp->length == len &&
			    memcmp(dp->name, arg, len) == 0)
				break;

		if (!dp->name)
			return -1;
		cat = dp->cat;
		p = (char*) arg + len;
	}

	if (*p == 0)
		lev = 100;
	else if (*p != '.')
		return -1;
	else {
		lev = strtoul(p + 1, &p, 10);
		if (*p)
			return -1;
	}
	debug_level[cat] = lev;
	return 0;
}


#define JOH_SEVERITY_DEBUG  0
#define JOH_SEVERITY_WARN   1
#define JOH_SEVERITY_INFO   2
#define JOH_SEVERITY_ERROR  3
#define JOH_SEVERITY_CRIT   4

static char *joh_severity_string[] = {
	"debug",
	"warning",
	"info",
	"error",
	"crit"
};

static void
vlogmsg_stderr(int severity, const char *file, unsigned line,
	       const char *fmt, va_list ap)
{
	fprintf(stderr, "%s: ", program_name);
	if (syslog_include_prio)
		fprintf(stderr, "%s: ", joh_severity_string[severity]);
	if (file)
		fprintf(stderr, "%s:%u: ", file, line);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
}

static int severity_to_prio[] = {
	LOG_DEBUG,
	LOG_WARNING,
	LOG_INFO,
	LOG_ERR,
	LOG_CRIT
};

struct kwtab {
	const char *name;
	int tok;
};
	
static struct kwtab facility_tab[] = {
  { "USER",    LOG_USER },
  { "DAEMON",  LOG_DAEMON },
  { "AUTH",    LOG_AUTH },
  { "AUTHPRIV",LOG_AUTHPRIV },
  { "MAIL",    LOG_MAIL },
  { "CRON",    LOG_CRON },
  { "LOCAL0",  LOG_LOCAL0 },
  { "LOCAL1",  LOG_LOCAL1 },
  { "LOCAL2",  LOG_LOCAL2 },
  { "LOCAL3",  LOG_LOCAL3 },
  { "LOCAL4",  LOG_LOCAL4 },
  { "LOCAL5",  LOG_LOCAL5 },
  { "LOCAL6",  LOG_LOCAL6 },
  { "LOCAL7",  LOG_LOCAL7 },
  { NULL }
};

static struct kwtab *
find_facility(const char *key)
{
	struct kwtab *p;

	for (p = facility_tab; p->name; p++)
		if (strcasecmp(p->name, key) == 0)
			return p;
	return NULL;
}

int
string_to_syslog_facility(const char *str, int *pfac)
{
	struct kwtab *kw = find_facility(str);
	if (kw) {
		*pfac = kw->tok;
		return 0;
	}
	return 1;
}	

static void
vlogmsg_syslog(int severity, const char *file, unsigned line,
	       const char *fmt, va_list ap)
{
	static char buffer[2048];
	size_t off = 0;

	if (syslog_include_prio) {
		strcpy(buffer, joh_severity_string[severity]);
		off = strlen(joh_severity_string[severity]);
		buffer[off++] = ':';
		buffer[off++] = ' ';
	}
	if (file)
		off += snprintf(buffer + off, sizeof buffer - off, "%s:%u: ",
				file, line);
	vsnprintf(buffer + off, sizeof buffer - off, fmt, ap);
	syslog(severity_to_prio[severity], "%s", buffer);
}

static void (*logger)(int severity, const char *file, unsigned line,
		      const char *fmt, va_list ap) = vlogmsg_stderr; 

void
joh_initlog()
{
	if (log_to_stderr)
		logger = vlogmsg_stderr;
	else {
		openlog(syslog_tag, LOG_PID, syslog_facility);
		logger = vlogmsg_syslog;
	}
}



void
verror(const char *fmt, va_list ap)
{
	logger(JOH_SEVERITY_ERROR, NULL, 0, fmt, ap);
}

void
error(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	verror(fmt, ap);
	va_end(ap);
}

void
die(int code, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	logger(JOH_SEVERITY_CRIT, NULL, 0, fmt, ap);
	va_end(ap);
	exit(code);
}

void
vwarn(const char *fmt, va_list ap)
{
	logger(JOH_SEVERITY_WARN, NULL, 0, fmt, ap);
}

void
warn(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vwarn(fmt, ap);
	va_end(ap);
}

void
logmsg(int severity, const char *file, unsigned line, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	logger(severity, file, line, fmt, ap);
	va_end(ap);
}


static const char *debug_file;
static int debug_line;

void
debug_begin_out(char *file, int line)
{
	if (syslog_source_info == 1) {
		debug_file = file;
		debug_line = line;
	}
}

void
debugf(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	logger(JOH_SEVERITY_DEBUG, debug_file, debug_line, fmt, ap);
	va_end(ap);
}

size_t
joh_format_vbuf(char vbuf[JOH_VBUFSIZE], const char *buf, size_t size)
{
	char *p = vbuf;
	char *q = vbuf + 51;
	int i;
	size_t j = 0;
	
	for (i = 0; i < 16; i++) {
		unsigned c;
		if (j < size) {
			c = *(const unsigned char*)buf++;
			j++;
		} else
			c = 0;
		sprintf(p, "%02X ", c);
		p += 3;
		*q++ = isprint(c) ? c : '.';
		if (i == 7) {
			*p++ = ' ';
			*q++ = ' ';
		}
	}
	*p++ = ' ';
	*p = ' ';
	*q = 0;
	return j;
}

void
joh_logdump(char **pfx, const char *buf, size_t size)
{
	char vbuf[JOH_VBUFSIZE];

	while (size) {
		size_t rd = joh_format_vbuf(vbuf, buf, size);
		if (pfx) {
			static char pfxbuf[1024];
			size_t off = 0;
			int i;
			
			for (i = 0; pfx[i] && off < sizeof(pfxbuf) - 2; i++) {
				size_t len = strlen(pfx[i]);

				if (off + len >= sizeof(pfxbuf) - 2)
					break;
				memcpy(pfxbuf + off, pfx[i], len);
				off += len;
				pfxbuf[off++] = pfx[i+1] ? ' ' : ':';
			}
			pfxbuf[off] = 0;
			debugf("%s %s", pfxbuf, vbuf);
		} else
			debugf("%s", vbuf);
		size -= rd;
		buf += rd;
	}
}


