/* connection table for johd.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

struct connection {
	struct connection *prev, *next;
	enum joh_conntype type;
	struct joh_sockaddr *sa;
	int inset;
};

size_t conncount;
struct connection *conntab, *conntab_head, *conntab_tail;

char *conn_type_str[] = {
	"unused",
	"master",
	"jabber",
	"client",
};

char *client_class_str[] = {
	"unused",
	"CGI",
	"HTTP",
	"TRANSPARENT"
};


#if defined(HAVE_SYSCONF) && defined(_SC_OPEN_MAX)
# define __getmaxfd() sysconf(_SC_OPEN_MAX)
#elif defined(HAVE_GETDTABLESIZE)
# define __getmaxfd() getdtablesize()
#elif defined OPEN_MAX
# define __getmaxfd() OPEN_MAX
#else
# define __getmaxfd() 256
#endif

int
getmaxfd()
{
	static int maxfd;
	if (maxfd == 0)
		maxfd = __getmaxfd();
	return maxfd;
}



void
joh_conntab_init()
{
	size_t i;
	conncount = getmaxfd();
	conntab = xcalloc(conncount, sizeof(conntab[0]));
	for (i = 0; i < conncount; i++)
		conntab[i].type = conn_unused;
}

enum joh_conntype
joh_connection_type(int fd)
{
	if (fd < 0 || fd >= conncount)
		return conn_unused;
	return conntab[fd].type;
}

enum joh_client_class
joh_connection_client_class(int fd)
{
	if (fd < 0 || fd >= conncount || !conntab[fd].sa)
		return client_unused;
	return conntab[fd].sa->class;
}

struct joh_sockaddr *
joh_connection_address(int fd)
{
	if (fd < 0 || fd >= conncount)
		return conn_unused;
	return conntab[fd].sa;
}

void
joh_connection_inset(int fd, int inset)
{
	if (fd < 0 || fd >= conncount)
		die(EX_SOFTWARE, "descriptor %d out of range 0..%d",
		    fd, conncount);
	conntab[fd].inset = inset;
}

void
joh_connection_register(int fd, enum joh_conntype type,
			struct joh_sockaddr *sa)
{
	struct connection *conn;
	
	if (fd < 0 || fd >= conncount)
		die(EX_SOFTWARE, "descriptor %d out of range 0..%d",
		    fd, conncount);

	conn = &conntab[fd];
	
	conn->type = type;
	conn->sa = sa;

	if (type == conn_unused) {
		struct connection *p;

		p = conn->prev;
		if (p)
			p->next = conn->next;
		else
			conntab_head = conn->next;
		p = conn->next;
		if (p)
			p->prev = conn->prev;
		else
			conntab_tail = conn->prev;
		conn->next = conn->prev = NULL;
	} else {
		conn->prev = conntab_tail;
		conn->next = NULL;
		if (conntab_tail)
			conntab_tail->next = conn;
		else
			conntab_head = conn;
		conntab_tail = conn;
	}
}

void
joh_connection_iterate(int (*itr)(int fd, enum joh_conntype type,
				  struct joh_sockaddr *sa, void *),
		       void *data, int all)
{
	struct connection *conn;

	for (conn = conntab_head; conn; conn = conn->next)
		if ((conn->inset || all) &&
		    itr(conn - conntab, conn->type, conn->sa, data))
			break;
}
