/* HTTP Polling support for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

int
joh_http_get_flush(enum joh_client_dir dir, struct joh_session *sess)
{
	switch (dir) {
	case joh_client_none:
		break;
	case joh_client_in:
		joh_chg_fd(sess->fd, FDMASK_WR);
		break;
	case joh_client_out:
		joh_session_destroy(sess);
	}
	return 0;
}
