/* Socket functions for johd.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

int joh_connect_init = 0;
fd_set joh_fd_set[_FDSET_MAX];
int joh_max_fd = -1;
int keep_existing_option = 0;

void
joh_add_fd(int fd, int mask)
{
	int i;

	for (i = 0; i < _FDSET_MAX; i++)
		if (FDMASK(i) & mask) {
			debug(JOH_DEBCAT_MAIN, 10,
			      ("adding FD %d to set %d", fd, i));
			FD_SET(fd, &joh_fd_set[i]);
		}
	if (fd > joh_max_fd)
		joh_max_fd = fd;
	joh_connection_inset(fd, 1);
}

void
joh_rem_fd(int fd)
{
	debug(JOH_DEBCAT_MAIN, 10,("removing FD %d", fd));
	FD_CLR(fd, &joh_fd_set[FDSET_RD]);
	FD_CLR(fd, &joh_fd_set[FDSET_WR]);
	if (fd == joh_max_fd)
		while (--joh_max_fd) {
			if (FD_ISSET(joh_max_fd, &joh_fd_set[FDSET_RD]) ||
		            FD_ISSET(joh_max_fd, &joh_fd_set[FDSET_WR]))
				break;
		}
	joh_connection_inset(fd, 0);
}

void
joh_chg_fd(int fd, int mask)
{
	int i;
	int inset = 0;
	
	for (i = 0; i < _FDSET_MAX; i++) {
		if (FDMASK(i) & mask) {
			inset = 1;
			debug(JOH_DEBCAT_MAIN, 10,
			      ("adding FD %d to set %d", fd, i));
			FD_SET(fd, &joh_fd_set[i]);
		} else {
			debug(JOH_DEBCAT_MAIN, 10,
			      ("removing FD %d from set %d", fd, i));
			FD_CLR(fd, &joh_fd_set[i]);
		}
	}
	if (fd > joh_max_fd)
		joh_max_fd = fd;
	joh_connection_inset(fd, inset);
}	

static int
setup_listen_socket(struct joh_sockaddr *sa, int fd)
{
	struct stat st;
	struct sockaddr_un *s_un;
	int t;

	switch (sa->addr->sa_family) {
	case AF_UNIX:
		s_un = (struct sockaddr_un *) sa->addr;
	
		if (stat(s_un->sun_path, &st)) {
			if (errno != ENOENT) {
				error("%s: file %s exists but cannot be stat'd: %s",
				      joh_sockaddr_str(sa),
				      s_un->sun_path,
				      strerror(errno));
				return -1;
			}
		} else if (!S_ISSOCK(st.st_mode)) {
			error("%s: file %s is not a socket",
			      joh_sockaddr_str(sa), s_un->sun_path);
			return -1;
		} else if (!keep_existing_option) {
			if (unlink(s_un->sun_path)) {
				error("%s: cannot unlink file %s: %s",
				      joh_sockaddr_str(sa), s_un->sun_path,
				      strerror(errno));
				return -1;
			}
		} else {
			error("socket `%s' already exists", s_un->sun_path);
			return -1;
		}
		break;

	case AF_INET:
		if (!keep_existing_option) {
			t = 1;	 
			setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
				   &t, sizeof(t));
		}
	}

	if (bind(fd, sa->addr, sa->addrlen) == -1) {
		error("%s: cannot bind: %s",
		      joh_sockaddr_str(sa), strerror(errno));
		return -1;
	}

	if (listen(fd, 8) == -1) {
		error("%s: listen: %s",
		      joh_sockaddr_str(sa), strerror(errno));
		return -1;
	}
	return 0;
}

static int
open_listen_socket(struct joh_sockaddr *sa)
{
	int fd = socket(sa->addr->sa_family, SOCK_STREAM, 0);
	if (fd == -1) {
		error("socket: %s", strerror(errno));
		return -1;
	}
	if (setup_listen_socket(sa, fd)) {
		close(fd);
		return -1;
	}
	joh_add_fd(fd, FDMASK_RD);
	joh_connection_register(fd, conn_master, sa);
	debug(JOH_DEBCAT_MAIN, 2, ("listening on socket %s",
				   joh_sockaddr_str(sa)));
	return 0;
}

void
joh_listen_socket_setup(struct joh_sockaddr *sa)
{
	size_t count = 0;
	
	if (!joh_connect_init) {
		FD_ZERO(&joh_fd_set[FDSET_RD]);
		FD_ZERO(&joh_fd_set[FDSET_WR]);
		joh_connect_init = 1;
	}
	for (; sa; sa = sa->next) {
		if (open_listen_socket(sa) == 0)
			count++;
	}
	if (count == 0)
		die(EX_CONFIG, "no listen sockets configured");
}

static int
_close_conn(int fd, enum joh_conntype type JOH_ARG_UNUSED, 
            struct joh_sockaddr *sa JOH_ARG_UNUSED, void *data JOH_ARG_UNUSED)
{
      close(fd);
      joh_connection_register(fd, conn_unused, NULL);
      return 0;
}

void
joh_listen_socket_cleanup()
{
	debug(JOH_DEBCAT_MAIN, 2, ("closing sockets"));
	joh_connection_iterate(_close_conn, NULL, 1);
	FD_ZERO(&joh_fd_set[FDSET_RD]);
	FD_ZERO(&joh_fd_set[FDSET_WR]);
	joh_max_fd = -1;
}
	
int
new_jabber_connection(struct joh_session *sess, struct joh_sockaddr **pjsa)
{
	int rc;
	int fd;
	struct sockaddr *sa;
	socklen_t salen;
	struct addrinfo *res = NULL;

	if (sess->param[joh_param_server].ptr &&
	    sess->param[joh_param_server].ptr) {
		rc = getaddrinfo(sess->param[joh_param_server].ptr,
				 sess->param[joh_param_port].ptr,
				 NULL, &res);
		if (rc) {
			error("invalid server address %s:%s: %s",
			      sess->param[joh_param_server].ptr,
			      sess->param[joh_param_port].ptr,
			      gai_strerror(rc));
			return -1;
		}
		sa = res->ai_addr;
		salen = res->ai_addrlen;
	} else {
		sa = jabber_addr->addr;
		salen = jabber_addr->addrlen;
	}

	if (!destination_allowed(sess->sa->srvname, sess->sa->addr, sa)) {
		char *str = sockaddr_str(sa, salen);
		error("%s/%s@%s: requested jabber server is not allowed (%s)",
		      conn_type_str[sess->type],
		      client_class_str[sess->sa->class],
		      joh_sockaddr_str(sess->sa),
		      str);
		free(str);
		errno = EACCES;
		return -1;
	}

	fd = socket(sa->sa_family, SOCK_STREAM, 0);
	if (fd == -1) {
		error("socket: %s", strerror(errno));
		if (res)
			freeaddrinfo(res);
		return -1;
	}
	/* FIXME: switch to non-blocking mode, and retry connects in
	   the main loop? */
	rc = connect(fd, sa, salen);
	if (rc) {
		char *s = sockaddr_str(sa, salen);
		error("cannot connect to %s: %s",
		      s, strerror(errno));
		free(s);
		close(fd);
		fd = -1;
	}
	*pjsa = joh_sockaddr_new(salen);
	memcpy((*pjsa)->addr, sa, salen);
	if (res)
		freeaddrinfo(res);
	return fd;
}

	
