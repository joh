/* johcgi: CGI gateway for Jabber-Over-HTTP.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

struct joh_param param[joh_param_max];
int fd;

void
cgi_error(int idcode, const char *text, const char *diag)
{
	if (diag)
		error("%s", diag);
	printf("Set-Cookie: ID=%d:0\r\n", idcode);
	printf("Content-Type: text/xml\r\n\r\n");
	if (text)
		printf("%s\r\n", text);
	exit(0);
}

void
dump_params()
{
	int i;

	for (i = 0; i < joh_param_max; i++)
		if (param[i].size)
			fprintf(stderr, "%u=%s\n", i, param[i].ptr);
}

void
clear_params()
{
	int i;
	for (i = 0; i < joh_param_max; i++) {
		free(param[i].ptr);
		param[i].ptr = NULL;
		param[i].size = 0;
	}
}


static void
fallback_jabber_data()
{
	char *s;

	s = getenv("JOH_JABBER_SERVER_URL");
	if (s) {
		char host[NI_MAXHOST];
		char srv[NI_MAXSERV];
		struct joh_sockaddr *jsa = parse_sockaddr(s, NULL);
		switch (jsa->addr->sa_family) {
		case AF_INET:
		case AF_INET6:
			if (getnameinfo(jsa->addr, jsa->addrlen,
					host, sizeof(host), srv, sizeof(srv),
					NI_NUMERICHOST|NI_NUMERICSERV))
				cgi_error(0,
					  "cannot parse address",
					  "cannot parse address");
			store_param(&param[joh_param_server],
				    host, strlen(host));
			store_param(&param[joh_param_port],
				    srv, strlen(srv));
			break;
			
		default:
			cgi_error(0,
				  "unsupported address family",
				  "unsupported address family");
		}

	} else {
		s = getenv("JOH_JABBER_SERVER");
		if (!s)
			s = JABBER_SERVER_NAME;
		store_param(&param[joh_param_server], s, strlen(s));
		s = getenv("JOH_JABBER_PORT");
		if (!s)
			s = JABBER_PORT_STRING;
		store_param(&param[joh_param_port], s, strlen(s));
	}
}

static void
jabber_data()
{
	char *query;

	query = getenv("QUERY_STRING");
	if (!query) {
		fallback_jabber_data();
		return;
	}
	if (parse_post_query(param, query))
		cgi_error(-2, "bad query", "bad query");
}


void
joh_connect()
{
	int rc;
	char *s;
	struct joh_sockaddr *jsa;
	struct sockaddr *sa;
	socklen_t salen;
	
	s = getenv("JOH_SERVER_URL");
	if (s)
		jsa = parse_sockaddr(s, NULL);
	else {
		static struct joh_sockaddr_hints
			conn_hints = { NULL, 
			               JOH_HINT_SERVICE,
				       DEFAULT_PORT_STRING };
		jsa = parse_sockaddr("inet://", &conn_hints);
	}
	sa = jsa->addr;
	salen = jsa->addrlen;
	
	fd = socket(sa->sa_family, SOCK_STREAM, 0);
	if (fd == -1) {
		error("socket: %s", strerror(errno));
		cgi_error(0, "connection error", NULL);
	}
	rc = connect(fd, sa, salen);
	if (rc) {
		error("cannot connect to %s: %s",
		      joh_sockaddr_str(jsa), strerror(errno));
		cgi_error(0, "connection error", NULL);
	}
}

void
resolve_server()
{
	int rc;
	struct addrinfo *res;
	char host[NI_MAXHOST];
	char srv[NI_MAXSERV];
	
	if (!(param[joh_param_server].ptr && param[joh_param_port].ptr))
		return;
		
	rc = getaddrinfo(param[joh_param_server].ptr,
			 param[joh_param_port].ptr,
			 NULL, &res);
	if (rc) {
		error("cannot resolve %s:%s: %s",
		      param[joh_param_server].ptr,
		      param[joh_param_port].ptr,
		      gai_strerror(rc));
		cgi_error(-1, NULL, NULL);
	}

	if (getnameinfo(res->ai_addr, res->ai_addrlen,
			host, sizeof(host), srv, sizeof(srv),
			NI_NUMERICHOST|NI_NUMERICSERV))
		cgi_error(-1, NULL, "cannot resolve server address");

	if (strcmp(param[joh_param_server].ptr, host)) {
		free(param[joh_param_server].ptr);
		store_param(&param[joh_param_server], host, strlen(host));
	}
	if (strcmp(param[joh_param_port].ptr, srv)) {
		free(param[joh_param_port].ptr);
		store_param(&param[joh_param_port], srv, strlen(srv));
	}
}

void
proxy()
{
	size_t content_length;
	char *content_ptr;
	char *s;
	joh_cbuf_t ibuf;
	struct joh_obuf obuf;
	ssize_t rc;
	char buf[IOBUFSIZE];

	s = getenv("CONTENT_LENGTH");
	if (!s)
		cgi_error(-2, NULL, NULL);

	content_length = strtoul(s, &s, 10);
	if (*s)
		cgi_error(-2, NULL, NULL);
	
	content_ptr = xmalloc(content_length + 1);
	fread(content_ptr, content_length, 1, stdin);
	content_ptr[content_length] = 0;
	if (parse_content(param, content_ptr, content_length))
		cgi_error(-2, NULL, "bad format");
	free(content_ptr);

	if (param[joh_param_id].size == 0)
		cgi_error(-2, NULL, "ID not set");

	jabber_data();
	resolve_server();
	joh_connect();

	/* Send out the pack */
	memset(&obuf, 0, sizeof(obuf));

	joh_pdu_encode_buf(&obuf, param);

	while (obuf.off < obuf.size) {
		rc = send(fd, obuf.base + obuf.off,
			  obuf.size - obuf.off,
			  0);
		if (rc < 0) {
			error("I/O error: %s", strerror(errno));
			cgi_error(-1, NULL, NULL);
		}
		obuf.off += rc;
	}
	free(obuf.base);

	/* Read and decode reply */
	ibuf = joh_cbuf_create();
	while ((rc = recv(fd, buf, sizeof(buf), 0)) > 0)
		joh_cbuf_append(ibuf, buf, rc);

	clear_params();
	if (joh_pdu_decode_buf(param, ibuf, "input"))
		cgi_error(0, NULL, "malformed input packet");

	/* Format HTTP reply */
	printf("Set-Cookie: ID=%s\r\n",
	       param[joh_param_id].ptr ? param[joh_param_id].ptr : "0");
	printf("Content-Type: text/xml\r\n\r\n");
	if (param[joh_param_payload].size)
		fwrite(param[joh_param_payload].ptr, 
		       param[joh_param_payload].size,
		       1, stdout);
}

char default_error_reply[] = 
#include "404.c"
;

void
error_page()
{
	char *s = getenv("JOH_ERROR_REDIRECT");
	if (s && strprefix(s, "http://"))
		printf("Location: %s\r\n\r\n", s);
	else {
		printf("Content-Type: text/html; charset=utf-8\r\n\r\n");
		s = getenv("JOH_ERROR_PAGE");
		if (s) {
			FILE *fp = fopen(s, "r");
			if (fp) {
				size_t n;
				char buf[IOBUFSIZE];
				while ((n = fread(buf, 1, sizeof(buf), fp)))
					fwrite(buf, 1, n, stdout);
				fclose(fp);
				return;
			}
		} 
		printf("%s", default_error_reply);
	}
}

int
main(int argc, char **argv)
{
	char *s;
	
	set_program_name(argv[0]);

	s = getenv("REQUEST_METHOD");
	if (s && strcmp (s, "POST") == 0)
		proxy();
	else
		error_page();
	return 0;
}
