/* johd: Main Jabber-Over-HTTP daemon.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"
#include <unistd.h>
#include <fcntl.h>

struct grecs_symtab *hint_table;

struct joh_sockaddr_hints *
joh_get_hint_by_name(const char *name, int install)
{
	struct joh_sockaddr_hints key;
	struct joh_sockaddr_hints *ent;

	if (!hint_table) {
		hint_table = grecs_symtab_create_default(
			            sizeof(struct joh_sockaddr_hints));
		if (!hint_table)
			grecs_alloc_die();
	}
  
	key.name = (char*) name;
	ent = grecs_symtab_lookup_or_install(hint_table, &key, &install);
	if (!ent && errno != ENOENT)
		grecs_alloc_die();
	return ent;
}

void
new_get_reply(struct joh_sockaddr_hints *hints, int redirect, const char *text)
{
	char *data;
	size_t length;

	if (redirect) {
		if (!strprefix(text, "http://"))
			die(EX_USAGE,
			    "argument to -R must begin with http://");
		data = grecs_strdup(text);
		length = strlen(text);
	} else {
		struct stat st;
		int fd;
		ssize_t rc;

		if (stat(text, &st))
			die(EX_USAGE,
			    "cannot open error file %s: %s",
			    text, strerror(errno));
		length = st.st_size;
		if (length != st.st_size)
			die(EX_USAGE,
			    "file %s is too big", text);
		data = xmalloc(length + 1);
		fd = open(text, O_RDONLY);
		if (fd == -1)
			die(EX_UNAVAILABLE,
			    "cannot open error file %s: %s",
			    text, strerror(errno));
		rc = read(fd, data, length);
 		if (rc != length)
			die(EX_UNAVAILABLE,
			    "error reading file %s: %s",
			    text, strerror(errno));
		/* FIXME: translate LF to CRLF? */
		close(fd);
	}
	
	hints->reply = xmalloc(sizeof(hints->reply[0]));
	hints->reply->redirect = redirect;
	hints->reply->data = data;
	hints->reply->length = length;
	hints->flags |= JOH_HINT_REPLY;
}


