/* PDU encoding/decoding routines for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

int
joh_pdu_decode(struct joh_session *sess)
{
	int rc;
	rc = joh_pdu_decode_buf(sess->param, sess->ibuf,
				joh_sockaddr_str(sess->sa));
	if (rc)
		return rc;
	/* force HTTP Polling protocol */
	sess->http_type = joh_http_poll;
	return joh_param_check(sess);
}
	
int
joh_pdu_encode(struct joh_session *sess JOH_ARG_UNUSED,
	       struct joh_obuf *buf, struct joh_param *param)
{
	return joh_pdu_encode_buf(buf, param);
}

