/* PDU encoding/decoding routines for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

/*
  PDU Format:
  
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |        Total PDU Length       |  Parameters ...
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

  Parameter Format:
  
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Length    |   Parameter   | Value ... 
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

char *param_str[] = {
	"id",
	"key",
	"newkey",
	"payload",
	"server",
	"port"
};

int
joh_pdu_decode_buf(struct joh_param param[joh_param_max],
		   joh_cbuf_t ibuf, const char *id)
{
	char *data, *ptr;
	uint32_t size;
	size_t cbufsize = joh_cbuf_size(ibuf);
	unsigned char i, nelem;
	
	if (cbufsize < sizeof(size))
		return 1;
	size = ntohl(*(uint32_t*) joh_cbuf_head(ibuf, NULL));
	if (cbufsize - sizeof(size) < size)
		return -1;
	data = joh_cbuf_finish(ibuf);
	data += sizeof(size);
	nelem = *data++;
	for (i = 0, ptr = data; i < nelem && ptr < data + size; i++) {
		unsigned char pidx;
		uint16_t len;

		len = ntohs(*(uint16_t*)ptr);
		if (len <= sizeof(len) + 1 || ptr + len >= data + size)
			return -1;
		ptr += sizeof(len);
		len -= sizeof(len);
		pidx = *(unsigned char *)ptr;
		
		if (pidx >= joh_param_max) {
			warn("ignoring unknown parameter %u", pidx);
			ptr += len;
			continue;
		}
		ptr++;
		len--;
		if (len) {
			char *p = xmalloc(len + 1);
			memcpy(p, ptr, len);
			p[len] = 0;
			param[pidx].ptr = p;
			param[pidx].size = len;
			debug(JOH_DEBCAT_PROTO, 2,
			      ("%s: %s=%s", id, param_str[pidx], p));
		} 
		ptr += len;
	}
	return 0;
}

int
joh_pdu_encode_buf(struct joh_obuf *buf, struct joh_param *param)
{
	char *ptr;
	size_t total, num;
	size_t i;
	
	total = sizeof(uint32_t) + 1;
	num = 0;
	for (i = 0; i < joh_param_max; i++)
		if (param[i].size) {
			num++;
			total += sizeof(uint16_t) + 1 + param[i].size;
		}
		
	buf->base = xmalloc(total);
	buf->base_alloc = 1;
	buf->size = total;
	buf->off = 0;

	ptr = buf->base;
	*(uint32_t*)ptr = htonl(total - sizeof(uint32_t));
	ptr += sizeof(uint32_t);
	*ptr++ = (unsigned char) num;

	for (i = 0; i < joh_param_max; i++) {
		size_t param_len;

		if (param[i].size == 0)
			continue;
		param_len = sizeof(uint16_t) + 1 + param[i].size;
		*(uint16_t*)ptr = htons(param_len);
		ptr += sizeof(uint16_t);
		*ptr++ = i;
		memcpy(ptr, param[i].ptr, param[i].size);
		ptr += param[i].size;
	}
	return 0;
}


