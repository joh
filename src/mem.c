/* Memory management for JOH.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"
#include <string.h>

void
xalloc_die()
{
	die(EX_SOFTWARE, "not enough memory");
}

void *
xmalloc(size_t size)
{
	void *p = malloc(size);
	if (!p)
		xalloc_die();
	return p;
}

void *
xrealloc(void *ptr, size_t size)
{
	void *newptr = realloc(ptr, size);
	if (!newptr)
		xalloc_die();
	return newptr;
}

void *
xcalloc(size_t nmemb, size_t size)
{
	void *p = calloc(nmemb, size);
	if (!p)
		xalloc_die();
	return p;
}

void *
xzalloc(size_t size)
{
	return xcalloc(1, size);
}

char *
xstrdup(char *str)
{
	char *p;

	if (!str)
		return NULL;
	p = xmalloc(strlen(str) + 1);
	strcpy(p, str);
	return p;
}
		
