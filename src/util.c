/* Various utility functions.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

void
store_param(struct joh_param *param, char *value, size_t len)
{
	if (len == 0)
		return;
	param->size = len;
	param->ptr = xmalloc(len + 1);
	memcpy(param->ptr, value, len);
	param->ptr[len] = 0;
}

static int
hex_to_num(int c)
{
	static char digits[] = "0123456789";
	static char dohex[] = "abcdef";
	static char uphex[] = "ABCDEF";
	char *p;
	
	if (p = strchr(digits, c))
		return p - digits;
	if (p = strchr(dohex, c))
		return p - dohex + 10;
	if (p = strchr(uphex, c))
		return p - uphex + 10;
	return 16;
}

int
unescape_query(char *q)
{
	char *p;

	p = strchr(q, '%');
	if (!p)
		return 0;
	for (q = p;;) {
		if (*q == '%') {
			unsigned char c1, c2;
			c1 = hex_to_num(q[1]);
			if (c1 == 16)
				return -1;
			c2 = hex_to_num(q[2]);
			if (c2 == 16)
				return -1;
			*p++ = c1*16 + c2;
			q += 3;
		} else if (!(*p++ = *q++))
			break;
	}
	return 0;
}
		
int
parse_post_query(struct joh_param *param, char *query)
{
	int stop = 0;

	do {
		char *val;
		size_t len = strcspn(query, ";");
		
		if (query[len])
			query[len++] = 0;
		else
			stop = 1;
		
		val = strchr(query, '=');
		if (val) {
			*val++ = 0;
			if (unescape_query(query) ||
			    unescape_query(val))
				return -1;
			
			if (strcmp(query, "server") == 0) {
				char *p = strchr(val, ':');
				if (p) {
					*p++ = 0;
					store_param(&param[joh_param_server],
						    val, strlen(val));
					store_param(&param[joh_param_port],
						    p, strlen(p));
				} else
					store_param(&param[joh_param_server],
						    val, strlen(val));
			} else if (strcmp(query, "port") == 0) {
				store_param(&param[joh_param_port],
					    val, strlen(val));
			}
		}
	} while (!stop);
	return 0;
}

int
parse_connect_query(struct joh_param *param, char *query)
{
	size_t n = strcspn(query, ":");

	store_param(&param[joh_param_server], query, n);
	if (unescape_query(param[joh_param_server].ptr)) {
		free(param[joh_param_server].ptr);
		param[joh_param_server].ptr = NULL;
		param[joh_param_server].size = 0;
		return -1;
	}
	param[joh_param_server].size = strlen(param[joh_param_server].ptr);

	query += n;
	if (query[0] && query[1]) {
		store_param(&param[joh_param_port],
			    query + 1, strlen(query + 1));
		if (unescape_query(param[joh_param_port].ptr)) {
			free(param[joh_param_port].ptr);
			param[joh_param_port].ptr = NULL;
			param[joh_param_port].size = 0;
			return -1;
		}
		param[joh_param_port].size = strlen(param[joh_param_port].ptr);
	}
	return 0;
}
	

/* XEP-0025:

   The client sends requests with bodies in the following format:

          identifier ; key [ ; new_key] , [xml_body]

   If the identifier is zero, key indicates an initial key. In this case,
   new_key should not be specified, and must be ignored.
*/

int
parse_content(struct joh_param *param, char *content_ptr, size_t content_length)
{
	size_t len;
	char *ptr = content_ptr;

	len = strcspn(ptr, ";");
	if (ptr[len] == 0)
		return -1;
	store_param(&param[joh_param_id], ptr, len);
	ptr += len + 1;

	len = strcspn(ptr, ",;");
	if (ptr[len] == 0)
		return -1;
	store_param(&param[joh_param_key], ptr, len);
	ptr += len;

	if (*ptr++ == ';') {
		len = strcspn(ptr, ",");
		if (ptr[len] == 0)
			return -1;
		store_param(&param[joh_param_newkey], ptr, len);
		ptr += len + 1;
	}

	store_param(&param[joh_param_payload], ptr,
		    content_length - (ptr-content_ptr));
	return 0;
}

void
joh_obuf_free(struct joh_obuf *obuf)
{
	if (obuf->base_alloc)
		free(obuf->base);
	obuf->base = NULL;
	obuf->base_alloc = 0;
	obuf->size = obuf->off = 0;
}

int
strprefix(const char *str, const char *pfx)
{
	size_t plen = strlen(pfx);
	return strlen(str) >= plen && memcmp(str, pfx, plen) == 0;
}

	    
