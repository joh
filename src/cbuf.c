/* johd: Character buffer.
   Copyright (C) 2011 Sergey Poznyakoff

   JOH is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   JOH is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JOH.  If not, see <http://www.gnu.org/licenses/>. */

#include "joh.h"

struct joh_cbuf_bucket {
	struct joh_cbuf_bucket *next;
	char *buf;
	size_t level;
	size_t size;
};

struct joh_cbuf {
	struct joh_cbuf_bucket *head, *tail;
	struct joh_cbuf_bucket *free;
};

static struct joh_cbuf_bucket *
alloc_bucket(size_t size)
{
	struct joh_cbuf_bucket *p = xmalloc(sizeof(*p) + size);
	p->buf = (char*)(p + 1);
	p->level = 0;
	p->size = size;
	p->next = NULL;
	return p;
}

static void
alloc_pool(joh_cbuf_t cbuf, size_t size)
{
	struct joh_cbuf_bucket *p = alloc_bucket(JOH_CBUF_BUCKET_SIZE);
	if (cbuf->tail)
		cbuf->tail->next = p;
	else
		cbuf->head = p;
	cbuf->tail = p;
}

static size_t
copy_chars(joh_cbuf_t cbuf, const char *str, size_t n)
{
	size_t rest;

	if (!cbuf->head || cbuf->tail->level == cbuf->tail->size)
		alloc_pool(cbuf, JOH_CBUF_BUCKET_SIZE);
	rest = cbuf->tail->size - cbuf->tail->level;
	if (n > rest)
		n = rest;
	memcpy(cbuf->tail->buf + cbuf->tail->level, str, n);
	cbuf->tail->level += n;
	return n;
}

joh_cbuf_t 
joh_cbuf_create()
{
	joh_cbuf_t cbuf = xmalloc(sizeof(*cbuf));
	cbuf->head = cbuf->tail = NULL;
	cbuf->free = NULL;
	return cbuf;
}

void
joh_cbuf_clear(joh_cbuf_t cbuf)
{
	if (cbuf->tail) {
		cbuf->tail->next = cbuf->free;
		cbuf->free = cbuf->head;
		cbuf->head = cbuf->tail = NULL;
	}
}	

void
joh_cbuf_free(joh_cbuf_t cbuf)
{
	struct joh_cbuf_bucket *p;
	if (cbuf) {
		joh_cbuf_clear(cbuf);
		for (p = cbuf->free; p; ) {
			struct joh_cbuf_bucket *next = p->next;
			free(p);
			p = next;
		}
	}
	free(cbuf);
}

void
joh_cbuf_append(joh_cbuf_t cbuf, const void *str, size_t n)
{
	const char *ptr = str;
	while (n) {
		size_t s = copy_chars(cbuf, ptr, n);
		ptr += s;
		n -= s;
	}
}

void
joh_cbuf_append_char(joh_cbuf_t cbuf, char c)
{
	joh_cbuf_append(cbuf, &c, 1);
}	

size_t
joh_cbuf_size(joh_cbuf_t cbuf)
{
	size_t size = 0;
	struct joh_cbuf_bucket *p;
	for (p = cbuf->head; p; p = p->next)
		size += p->level;
	return size;
}

size_t
joh_cbuf_coalesce(joh_cbuf_t cbuf)
{
	size_t size;

	if (cbuf->head && cbuf->head->next == NULL)
		size = cbuf->head->level;
	else {
		struct joh_cbuf_bucket *bucket;
		struct joh_cbuf_bucket *p;

		size = joh_cbuf_size(cbuf);
	
		bucket = alloc_bucket(size);
		for (p = cbuf->head; p; ) {
			struct joh_cbuf_bucket *next = p->next;
			memcpy(bucket->buf + bucket->level, p->buf, p->level);
			bucket->level += p->level;
			free(p);
			p = next;
		}
		cbuf->head = cbuf->tail = bucket;
	}
	return size;
}

void *
joh_cbuf_head(joh_cbuf_t cbuf, size_t *psize)
{
	if (psize) 
		*psize = cbuf->head ? cbuf->head->level : 0;
	return cbuf->head ? cbuf->head->buf : NULL;
}

void *
joh_cbuf_finish(joh_cbuf_t cbuf)
{
	joh_cbuf_coalesce(cbuf);
	joh_cbuf_clear(cbuf);
	return cbuf->free->buf;
}
