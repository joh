# This file is part of JOH.
# Copyright (C) 2011 Sergey Poznyakoff
# Distributed under the terms of the GNU General Public License, either
# version 3, or (at your option) any later version. See file COPYING
# for the text of the license.

# Provide an initial comment and leading quote
1i\
/* This file is generated automatically, do not edit. */\
"\\

# Provide trailing quote
$a\
"

# Remove empty lines and special comments
/<!--#/,/#-->/d
/^ *$/d
# Escape quotes and backslashes
s/["\]/\\&/g
# Add newline and continuation character at the end of each line
s/$/\\n\\/
# End

