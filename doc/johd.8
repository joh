.\" This file is part of JOH. -*- nroff -*-
.\" Copyright (C) 2011 Sergey Poznyakoff
.\"
.\" JOH is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" JOH is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with JOH.  If not, see <http://www.gnu.org/licenses/>.
.TH JOHD "8" "March 31, 2011" "JOH" ""
.SH NAME
johd \- Jabber Over HTTP daemon

.SH SYNOPSIS
\fBjohd\fR [\-hVDiP] [\-c \fICLASS\fR] [\-S \fINAME\fR]\
 [\-t \fICLASS\fR:\fITIMEOUT\fR] [\-d \fINUMBER\fR]
     [\-l \fIURL\fR] [\-s \fIURL\fR] [-p \fIPIDFILE\fR]\
 [\-L \fITAG\fR] [\-F \fIFACILITY\fR]
     [\-E \fIFILE\fR] [\-R \fIURL\fR] [\-u \fIUSER\fR]

.SH DESCRIPTION
\fBJohd\fR is a proxy server which provides HTTP tunnel for Jabber
connections.  Such a tunnel is necessary for users located behind
firewalls which do not allow outbound connections to the Jabber client
port 5222/tcp.

This manpage is a short description of the \fBjohd\fR daemon.
For a detailed discussion, including examples of the configuration and
usage recommendation, refer to the \fBJOH User Manual\fR available in
Texinfo format.  To access it, run:

  \fBinfo joh\fR

Should any discrepancies occur between this manpage and the
\fBJOH User Manual\fR, the later shall be considered the authoritative
source.
  
When started, \fBjohd\fR listens on a set of network interfaces for
incoming HTTP requests, extracts and assembles XMPP packets, and forwards
them to appropriate Jabber server.  When the server responds, a reverse
operation is performed: the received data are stored in an internal queue
until a matching HTTP request arrives, whereby the data are converted to
HTTP and sent back to the requester.  This operation mode is called
\fIHTTP Polling\fR.

Another mode is \fIHTTP CONNECT\fR.  If the client sends a 
\fBCONNECT\fR request, its argument must be IP address and 
port number separated with a colon.  In this case, \fBjohd\fR connects
to that server and further acts as a transparent proxy, forwarding
packets between the client and Jabber server.

In \fIHTTP Polling\fR mode, a usual HTTP server, such as Apache, can
be used for handling incoming connections.  An auxiliary CGI program
\fBjoh.cgi(8)\fR is provided to that effect.  If this case, \fBjohd\fR
is configured to listen on a custom TCP port for requests coming from
this program.  When started without options, \fBjohd\fR assumes this
mode and listens on the default port 1100.

Both methods of operation can be combined: \fBjohd\fR can listen on any
number of HTTP and custom ports simultaneously.

Each incoming connection is validated against TCP wrappers, using
service name \fBjohd\fR, which can be changed using the \fB-S\fR
option (see below).  Requested Jabber server addresses are also
validated, using service name \fBjohd/jabber@\fIIP\fR, where \fIIP\fR
is the IP address of the server in question, in numeric form.

.SH OPTIONS
.TP
\fB\-D\fR
Daemon mode.  After startup \fBjohd\fR disconnects from the
controlling terminal and works in the background.
.TP
\fB\-d\fR \fINUMBER\fR
Sets debugging level.  The argument is a decimal integer number in the
range \fB0..100\fR.  The level of 100 forces maximum amount of debugging
information, including dumps of network packets sent and received.
.TP
\fB-c\fR \fICLASS\fR
Sets socket class.  Allowed values for \fICLASS\fR are \fIHTTP\fR, for
handling HTTP requests and \fICGI\fR, for handling requests from \fBjoh.cgi(8)\fR.

This option affects all subsequent \fB-l\fR options appearing to the right
of it, until another \fB-c\fR option or end of command line is encountered,
whichever occurs first.
.TP
\fB-E\fR \fIFILE\fR
Read the 404 error page from \fIFILE\fR.  This error page is returned as
a response to \fBGET\fR HTTP requests. 

This option affects all HTTP sockets created by subsequent \fB-l\fR options
which appear to the right of it, until another \fB-E\fR option or end of
command line is encountered, whichever occurs first.
.TP
\fB-F\fR \fIFACILITY\fR
Sets syslog facility.
.TP
\fB\-i\fR
Shows source line information along with debug messages.
.TP
\fB\-L\fR \fISTRING\fR
Sets log tag.
.TP
\fB\-l\fR \fIURL\fR
Listen on the given \fBURL\fR.  Any number of \fB\-l\fR options can be
specified to listen on several interfaces.  See the \fBURL\fR
section below, for a discussion of \fIURL\fR syntax.
.TP
\fB\-P\fR
Prefix diagnostic messages with their severity level.
.TP
\fB\-p\fR \fIFILE\fR
Write PID to \fIFILE\fR.  If the \fB\-u\fR option (see below) is given
together with this option, the file is written after switching to the
privileges of the user.  You should make sure the directory were the
\fIFILE\fR is to be written is writable for the user supplied with
\fB\-u\fR.
.TP
\fB\-R\fR \fIURL\fR
Redirect \fBGET\fR requests to \fIURL\fR.

This option affects all HTTP sockets created by subsequent \fB-l\fR options
which appear to the right of it, until another \fB-R\fR option or end of
command line is encountered, whichever occurs first.
.TP
\fB\-S\fR \fINAME\fR
Sets service name for TCP wrappers.  This option affects all
subsequent \fB-l\fR options appearing to the right of it, until
another \fB-S\fR option or end of command line is encountered,
whichever occurs first.

Addresses of the requested Jabber servers are validated using 
service name \fINAME\fR\fB/jabber@\fR\fIIP\fR, where \fIIP\fR
is the IP address of the server in question, in numeric form.
.TP
\fB\-s\fR \fIURL\fR
Sets default jabber server address.  This address is used for CGI requests
that lack explicit server and port specifications. 
.TP
\fB\-t\fR \fICLASS\fR:\fITIMEOUT\fR
Sets session idle timeout.  \fICLASS\fR is \fBJ\fR, for Jabber sessions, and
\fBC\fR, for CGI sessions.  \fITIMEOUT\fR is the desired timeout value, either
as a decimal number, which is treated as seconds, or as a time specification
in the form of \fIN1\fRh\fIN2\fRm\fIN3\fRs.  In the latter case, any one
of the three \fINc\fR parts is optional, but at least one of them must be
present.  For example, \fI-t J:10m\fR sets a timeout of 10 minutes for all
Jabber sessions.

Default timeout values are: 5 minutes for Jabber, and 1 minute for CGI
client sessions.
.TP
\fB\-u\fR \fIUSER\fR
Switch to \fIUSER\fR's privileges after completing privileged
operations, such as creating sockets that listen on ports below 1024.

Notice, that pidfile, if requested, is written with the new privileges.
.TP
\fB\-V\fR
Prints program version and copyright information.
.TP
\fB\-h\fR
Shows a terse help summary.

.SH URL
\fBJohd\fR is able to handle three socket families: UNIX sockets, IPv4 and
IPv6.  URLs provide a uniform way of specifying socket addresses in any of
these families.

A URL consists of the following parts:

  \fIscheme\fR://\fIaddress\fR:\fIport\fR

Up to two parts can be omitted, if that does not create ambiguity.

Valid URL schemes are:
.TP
\fBunix\fR
Specifies a UNIX socket.  The \fIaddress\fR part is the socket pathname,
the \fI:port\fR part is not used.  The pathname must be absolute, e.g.:

unix:///var/run/joh.socket

To facilitate typing, the two slashes after the colon can be omitted, e.g.:

unix:/var/run/joh.socket

.TP
\fBinet\fR
Specifies an IPv4 socket.  The \fIaddress\fR part must be an IPv4 address
in dotted quad form, or a host name.  If the latter resolves to multiple
addresses, those belonging to the IPv4 family are selected.  The \fIport\fR
part is either the network port number in decimal, or a corresponding
service name from \fI/etc/services\fR.  For example:

 inet://127.0.0.1:1100

.TP
\fBinet6\fR
Specifies an IPv6 socket.  The \fIaddress\fR part must be either an
IPv6 address in numeric notation enclosed in square brackets or a host
name.  If the latter resolves to multiple addresses, those belonging
to the IPv6 family are selected.  The \fIport\fR part is either the
network port number in decimal, or a corresponding service name from
\fI/etc/services\fR.  E.g.:

  inet6://[::1]:1100

If the URL is argument to the \fB-l\fR option, either \fIaddress\fR or
\fIport\fR can be omitted.  If \fIaddress\fR is omitted, the program
will listen on all available network interfaces with addresses from the
specified family, e.g.:

  inet6://:1100

instructs \fBjohd\fR to listen on port 1100 on all IPv6 interfaces.

If \fIport\fR is omitted, the default is selected depending upon the
class of the socket.  Default port for \fBCGI\fR sockets is 1100.
Default port for \fBHTTP\fR sockets is 80:

  inet6://[::1]
  inet://127.0.0.1

The \fIport\fR component can also be omitted in URLs which are
arguments to the \fB-s\fR option.  In this case, port defaults to 5222.

For compatibility with earlier versions, \fBjohd\fR accepts IPv6
addresses without square brackets, although such use is not
recommended, e.g.:

  inet6://::1:1100

If the \fIscheme\fR part is omitted, \fBjohd\fR tries to do its best
to guess what address family is assumed.  Thus:

\fI/var/run/socket\fR is treated as \fIunix:///var/run/socket\fR;

\fI127.0.0.1\fR is treated as \fIinet://127.0.0.1\fR;

\fI[::1]:3398\fR is treated as \fIinet6://[::1]:3398\fR;

\fI::1:3398\fR is treated as \fIinet6://[::1]:3398\fR;

\fI3456\fR causes \fBjohd\fR to listen on port 3456 on all available
network interfaces, no matter what their address family is.

.SH AUTHORS
Sergey Poznyakoff

.SH "BUG REPORTS"
Report bugs to <gray+joh@gnu.org.ua>.

.SH "SEE ALSO"
joh.cgi(8),
tcpd(8),
hosts_access(5), format of the tcpd access control tables.
services(5)

.SH COPYRIGHT
Copyright \(co 2011 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH JOHD \"8\" \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 16
.\" end:

